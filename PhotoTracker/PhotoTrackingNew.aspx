<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhotoTrackingNew.aspx.cs" Inherits="PhotoTracker.PhotoTrackingNew" %>
<%@ Register Assembly="Bytescout.BarCode" Namespace="Bytescout.BarCode" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>PhotoTrack</title> 
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet" />
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            
            function Comments()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('Comments.aspx?strItmNum=' + itemnum, 'Comments', 'width=600,height=600,resizable=yes');
            }
 
          $(document).ready(function() 
          {
             $(document.body).delegate('.alters a', 'click', function()
	         {
                var image = $(this).attr("rel");
			    var src = $('.productimg img').attr("src") == image;
	           $('.productimg').html('<img src="' + image + '" style="height: 520px; width: 370px;" />').fadeOut(100).fadeIn(150);
             }
          );
         });
 

        function mainthumb_onclick(strName) {
         document.getElementById("imgPath").value = strName;
        }
		</script>
		
	</head>
		<body onload="javascript:document.Form1.txtItemNum.focus();"  bgcolor="#000000">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
<table class="innerTableDetail" id="tblOption" cellspacing="0" cellpadding="0" width="96%" align="center" border="0" runat="server">
					<tbody>
						<tr>
							<td class="innerTableDetail" colspan="2">
								<table cellspacing="0" cellpadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<tbody>
										<tr>
											<td colspan="2" style="width: 587px">
												<table width="80%" border="0">
													<tr>
														<td class="tableTitle" colspan="2" valign="middle">Items Details - Photo Tracking</td>
													</tr>
													<tr style="height: 30px">
														<td style="width: 587px">
															<table style="width: 587px" border="0">
																<tr>
																	<td style="width: 60px">Item Input: </td>
                                                                    <td style="width: 70px"><asp:textbox id="txtItemNum" runat="server" CssClass="tableInput" columns="10" OnTextChanged="txtItemNum_TextChanged"></asp:textbox></td>
                                                                    <td style="width: 62px">&nbsp;Description:</td>
                                                                     <td><asp:textbox ReadOnly ="True" id="txtDescription" runat="server" CssClass="tableInput" columns="48"></asp:textbox></td>
																</tr>
																
																<tr><td colspan="4" style="width: 587px"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
																</tr>
																
															</table>
                                                            </td>
													</tr>
													<tr style="height: 20px"><td style="width: 587px"><table border="0">
													            <tr style="height: 4px"><td style="width: 38px">IN BY:</td>
													            <td style="width: 160px"><asp:textbox id="txtReceivIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="33"></asp:textbox></td><td width="10"> </td>
													            <td>SAMPLE IN:</td><td><asp:textbox id="txtSampleDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="30"></asp:textbox></td></tr>
													</table>
                                                        </td></tr>
												</table>
                                               
											</td>
											<td valign ="top" style="width:400px">
												<table cellspacing="0" cellpadding="0" width="80%" border="0">
													<tr>
																	
																	<td style="HEIGHT: 80px; width: 120px;" align="left">
                                                                        <strong> </strong>
                                                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="APPROVAL DATE"></asp:Label>
                                                                        <asp:TextBox ID="apprDt" runat="server" Width="90px"></asp:TextBox>
                                                                        &nbsp; &nbsp;
																	</td>
																	<td style="HEIGHT: 80px; width: 300px;" align="left">
                                                                        <strong>&nbsp; </strong>
																		<asp:radiobuttonlist id="rdTopSam" runat="server" AutoPostBack="True" ToolTip="Please select TOPS or SAMPLE item type." OnSelectedIndexChanged="rdTopSam_SelectedIndexChanged1">
																		<asp:listitem value="TOPS">TOPS</asp:listitem>
																	    <asp:listitem value="SAMPLE">SAMPLE</asp:listitem></asp:radiobuttonlist>
																	</td>
																	<td style="height: 80px"></td>
																	</tr>
												</table>
                                                <br />
                                                </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" valign="top" style="width: 267px">
								<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
									<tr style="height: 23px"><td style="width: 400px" valign="middle"><table>
									     <tr valign="bottom"><td class="tableTitle" style="HEIGHT: 23px; width: 300px" colspan="2">Entry &nbsp;Information</td>
									     <td align="right" valign="bottom"><font class="fontSmallBold">( Format: MM/DD/YYYY )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>
                                                &nbsp; &nbsp; &nbsp;</strong></font></td>
									</tr></table>
                                    </td></tr>
                                    <tr style="font-weight: bold; font-family: Arial"><td style="width: 350px; height: 115px;" class="innerTableDetailThin" valign="middle">
                                        <table border="0" cellspacing="0" cellpadding="0" id="TABLE3" style="width: 430px">
									<tr style="height: 50px">
										<td class="tableCell" style="width: 435px; height: 30px" >
                                            &nbsp;
                                        <asp:Label Font-Bold="True"  ID="Label5" runat="server" Text="Stylist Check-in:" />
                                            &nbsp;
                                        <asp:CheckBox ID="stlstChk" runat="server" OnCheckedChanged = "stlstChk_CheckedChanged" AutoPostBack="true"/>
										  <table border="0">
													            <tr style="height: 20px">
													            <td valign="bottom" style="height: 20px; width: 211px;"><asp:textbox id="txtStylistChkIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="33" Width="206px"></asp:textbox></td>
													            <td valign="bottom" style="height: 20px;" ><asp:textbox id="txtStylistChkInDt" ReadOnly ="True" runat="server" CssClass="tableInput" columns="30"></asp:textbox></td></tr>
													</table>
                                            <br /></td>
									 </tr>
									
									</table>
                                        &nbsp;
                                    </td></tr> 						            
						            <tr style="font-weight: bold; font-family: Arial"><td style="width: 350px; height: 115px;" class="innerTableDetailThin" valign="middle">
                                        <table border="0" cellspacing="0" cellpadding="0" id="TABLE2" style="width: 430px">
									<tr style="height: 50px">
										<td class="tableCell" style="width: 435px; height: 30px" >
                                            &nbsp;
                                        <asp:Label Font-Bold="True"  ID="Label2" runat="server" Text="Photo Complete:" />
                                            &nbsp;
                                        <asp:CheckBox ID="phComplete" runat="server" OnCheckedChanged = "phCompleteChkd_CheckedChanged" AutoPostBack="true"/>
										  <table border="0" cellspacing="0" cellpadding="0">									         
									         <tr style="height: 20px">
     						                   <td valign="bottom" style="height: 20px; width: 211px;">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
     						                         <asp:dropdownlist id="ddlPhotoOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhotoOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" style="height: 20px;" >
						                          <asp:textbox id="txtPhotoDateOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.ddlFinalOut');"
								href="javascript:;"><img style="height: 12px" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									          </td>
									         </tr>
									         </table>
                                            <br />
                                            &nbsp;
									         <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Video:"></asp:Label>
                                            &nbsp;
                                            <asp:CheckBox ID="videoChkd" runat="server" OnCheckedChanged = "videoChkd_CheckedChanged" AutoPostBack="true"/>
                                            <br /><asp:Label ID="lblvdoerrormsg" runat="server" Font-Bold="False" Visible="False" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblvdolink" runat="server" Font-Bold="False" Visible="False" ForeColor="Red"></asp:Label></td>                                                                                        
                                            
									 </tr>
									
									</table>
                                        &nbsp;
                                    </td></tr>  
									<tr><td style="width: 350px; height: 107px;" class="innerTableDetailThin" valign="middle">
                                        &nbsp;<table border="0" cellspacing="0" cellpadding="0" style="height: 53px; width: 435px;">
									<tr style="height: 50px">
										<td class="tableCell" style="width: 350px; height: 50px">
                                            &nbsp;&nbsp;
                                        <asp:Label Font-Bold="True" ID="Label4" runat="server" Text="Photo Edit Complete:" Width="120px" />
                                        &nbsp;
                                        <asp:CheckBox ID="phEditComplete" runat="server" AutoPostBack="True" OnCheckedChanged="phEditComplete_CheckedChanged" /><br />
										  <table border="0" cellspacing="0" cellpadding="0">
									         
									         <tr style="height: 20px">
     						                   <td valign="bottom" style="height: 10px; width: 231px;" >
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:dropdownlist id="ddlPhEditOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhEditOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" style="height: 10px">
						                          <asp:textbox id="txtPhEditOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditOut');"
								href="javascript:void(0);"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									
									          </td>
									         </tr>
									         </table>
									          
									      </td>
									 </tr>
									</table><table border="0" cellspacing="0" cellpadding="0" id="Table4" style="width: 430px">
                                        <tr style="height: 50px">
                                            <td class="tableCell" style="width: 467px; height: 30px" >
                                                &nbsp;
                                                <asp:Label Font-Bold="True"  ID="Label6" runat="server" Text="WhiteBack Image:" />
                                                &nbsp;
                                                <asp:CheckBox ID="whiteBackImageChk" runat="server"  AutoPostBack="True" />
                                            </td>
                                        </tr>
                                    </table>
                                    </td></tr>

										  <tr valign="bottom"><td style="width: 350px" valign="middle">
									  <table><tr><td style="width: 300px" valign="top">
									         <table><tr><td valign="top" style="width: 50px"><a href="javascript:Comments()"><img src="./Image/Comments.gif" border="0" alt="" style="width: 102px" height="28" />
                                           </a>                                           
									       </td>

                                           <td valign="top" style="width: 35px">
                                           <asp:panel runat="server" id="pnlUpdate" visible="true"><table><tr><td valign="top" style="height: 28px"><asp:Button id="btnUpdate" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnUpdate_Click" Height="28px"></asp:Button></td></tr></table></asp:panel>
                                           <asp:panel runat="server" id="pnlRecv" visible="false"><table><tr><td valign="top" style="height: 28px"><asp:Button id="btnRecv" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnRecv_Click" Height="28px"></asp:Button></td></tr></table></asp:panel>
									       <asp:panel runat="server" id="pnlEntry" visible="false"><table><tr><td valign="top" style="height: 28px"><asp:Button id="btnEntry" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnEntry_Click" Height="28px"></asp:Button></td></tr></table></asp:panel>
                                           </td>
									                    <td valign="top" style="width: 35px"><asp:Button id="btnClear" runat="server" Text="CLEAR" CssClass="buttonRed" OnClick="btnClear_Click" Height="28px" Width="55px"></asp:Button></td>
                                           </tr>									                
									         </table>
															</td>
									             <td><cc1:barcodewebimage id="BarcodeWebImage2" runat="server" RenderingHint="SystemDefault" SmoothingMode="Default" Value="DATA" ForeColor="Black" HorizontalAlignment="Right" Angle="Degrees0" CaptionPosition="Below" NarrowBarWidth="2" VerticalAlignment="Top" AddChecksum="False" AddChecksumToCaption="False" AdditionalCaptionPosition="Above" AdditionalCaptionFont="Arial, 10pt" BackColor="White" BarHeight="20" CaptionFont="Arial, 10pt" DrawCaption="True" WideToNarrowRatio="3" Symbology="Code128" AdditionalCaption=" " EnableTheming="True"></cc1:barcodewebimage></td>
									  </tr></table>
                                      </td></tr>   
                                      <tr><td style="width: 350px; height: 30px;" valign="middle"><font color="#cc0000"><asp:Label ID ="lblComExist" runat="server"></asp:Label><br />
                                      </font></td></tr> 
								</table>
								<table cellspacing="0" cellpadding="1" style="width: 100px; height: 10px" align="left" border="2">
									<tbody>
										<tr>
											<td class="tableTitle" style="height: 5px" colspan="2">Item&nbsp;Information</td>
										</tr>
										<tr valign="top" style="height: 5px">
											<td class="innerTableDetailThinTiny" align="center" style="width: 170px; height: 5px">
                                                <br />
												<table border="0" align="center" style="height: 35px; width : 250px" >
													<tr style="height: 5px">
														<td style="height: 5px; width: 141px;">&nbsp;DIVISION:</td><td style="width: 114px; height: 8px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtDivision" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px; width: 141px;">&nbsp;DEPARTMENT:</td><td style="width: 114px; height: 10px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtDept" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px;width: 141px">&nbsp;CLASS:</td><td style="width: 114px">&nbsp;<asp:textbox ReadOnly ="True" id="txtClass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px;width: 141px">&nbsp;SUBCLASS:</td><td style="width: 114px">&nbsp;<asp:textbox ReadOnly ="True" id="txtSubclass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													 
													<tr>
														<td style="height: 38px;width: 141px">&nbsp;PRICE:</td><td style="width: 114px; height: 38px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtPrice" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													<tr>
														<td style="width: 141px; height: 25px;">CANCEL DATE:</td><td style="width: 120px; height: 25px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtCancelDate" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
												</table>
                                                </td>
                                                <td style="width: 170px; height: 5px"><table border="0" class="innerTableDetailThinTiny">
                                                   
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 1</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme1" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
                                                  
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 2</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme2" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 3</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme3" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 4</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme4" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 5</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme5" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;PRIV. LABEL</td>
													</tr>
													<tr>
												         <td style="width: 156px; height: 26px;">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtPriLabel" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													
													</table></td>
										</tr>
									  
									</tbody>
									
								</table>
                               </td>
							<td class="innerTableDetail" valign="top" style="width: 350px" align="center">
                                <table cellspacing="0" cellpadding="1" width="100%" align="center" border="0">
                                <tr><td colspan="3" class="innerTableDetailThinTiny" style="height: 620px; width: 351px;">
								<div>	
                                                <asp:TextBox ID="itemTitle" runat="server" BorderStyle="Solid" Width="305px" ReadOnly="True" ForeColor="#404040"></asp:TextBox><br />
                                    <br />
                                    <table style="width: 350px; height: 610px">
									<tr>
       <td style="height: 605px; width: 200px" valign="top">   
           <%  if (bShowImg == false)
         {
      %>     
      <div class="productimg">
          <asp:Image id="Image52" runat="server" ImageUrl="/PhotoTracker/Image/noimage.gif"></asp:Image></div>              
      
      <%
           }
          else  
          {
           %>
            <div class="productimg">
          <img src="<%=strItmPrimaryImg%>"  style="visibility:visible; height: 500px;width: 350px;"  name="mainthumb"  border="1"  alt="default image" />
       </div> 
       <%
       }
       %>
      </td>
	 </tr></table></div>
	</td></tr>
  </table>
     <ul class="alternate_img"> 
    
       <%
       
           foreach (string strImgTp in listImages)
           {
           %>
           <li class="alters"><a rel="<%=strItmNum%><%=strImgTp%><%=strZoomedParams%>" > 
           <img src="<%=strItmNum%><%=strImgTp%>"  style="visibility:visible"  name="mainthumb" width="35"  id="Img1"  border="0" alt="" onclick="mainthumb_onclick(this.src)" height="40" /> </a> 
           <%
           }
           %>
</li>     
</ul> 
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="IMAGE PATH"></asp:Label><br />
                                <asp:TextBox ID="imgPath" runat="server" Height="14px" Width="380px" Enabled="False" ReadOnly="True"></asp:TextBox><br />
                                <asp:TextBox ID="primeImgPath" runat="server" style="display:none;"></asp:TextBox> 
                                <br />
                                <br />
                            </td></tr>
  </tbody>
 </table>
 </div>
</div>
</form>
</body>
</html>

