using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class Comments : System.Web.UI.Page
    {
        protected CommentGroup newCommentGroup;
        protected string strItemNum = string.Empty;
        public DataTable dtUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            strItemNum = Request.QueryString["strItmNum"];
            ViewState["StrItemNum"] = strItemNum;

            LoadComments(strItemNum);
 
        }

        protected void dlItemView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Write("<script language='javascript'> { self.close() }</script>");
        }

        protected void btnSaveNotes_Click(object sender, EventArgs e)
        {
            string strItemNum = string.Empty;
            int nComResult = 0;
            int intEmployeeID = 0;
            strItemNum = ViewState["StrItemNum"].ToString();
            if (strItemNum != "")
            {
                dtUser = (DataTable)Session["UserProfile"];
                if (dtUser != null)
                {
                    intEmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());

                    nComResult = Data.CommentInsert(strItemNum, intEmployeeID, txtComments.Text.ToString());

                    if (nComResult == 1)
                    {
                        txtComments.Text = string.Empty;
                        LoadComments(strItemNum);
                    }
                    else
                    {
                        //insert failed
                        lblNoItemMsg.Text = "Could not save Item # " + strItemNum + " to Database";
                    }
                }
                else
                {
                    //could not find empID
                    lblNoItemMsg.Text = "Could not find empployee name for Item # " + strItemNum + " on Database.";
                }
            }
            else
            {
                lblNoItemMsg.Text = "Please input an item number on PhotoTracking.aspx page first.";
            }
        }

        private void LoadComments(string strItemNum)
        {
            newCommentGroup = CommentGroup.GetCommentsDataTable(strItemNum);

            dlItemView.DataSource = newCommentGroup;

            dlItemView.DataBind();

        }
    }
}
