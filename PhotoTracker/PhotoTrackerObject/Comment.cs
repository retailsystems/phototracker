using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


namespace PhotoTracker
{
    public class Comment : CollectionBase
    {

        #region Members

        string _strItemNum = String.Empty;
        string _strEmployeeID = String.Empty;
        string _strEmpName = String.Empty;

        DateTime _dtCommentDate;
        string _strComments = String.Empty;
       
        #endregion Members

        #region Properties
        public string StrItemNum
        {
            get { return _strItemNum; }
            set { _strItemNum = value; }
        }

        public string StrEmployeeID
        {
            get { return _strEmployeeID; }
            set { _strEmployeeID = value; }
        }

        public string StrEmpName
        {
            get { return _strEmpName; }
            set { _strEmpName = value; }
        }
        public DateTime DtCommentdDate
        {
            get { return _dtCommentDate; }
            set { _dtCommentDate = value; }
        }

        public string StrComments
        {
            get { return _strComments; }
            set { _strComments = value; }
        }
        
        #endregion Properties

        public Comment()
        {
        }

        public void PopulateItemProperties(SqlDataReader dr)
        {
            _strItemNum = Convert.ToString(dr["ItemNum"]);
            _strEmployeeID = Convert.ToString(dr["EmployeeID"]);

            if (dr["CommentDate"] != System.DBNull.Value)
            {
                _dtCommentDate = Convert.ToDateTime(dr["CommentDate"]);
            }
            else
            {
                _dtCommentDate = DateTime.Parse("1/1/1980");
            }
            _strEmpName = Convert.ToString(dr["EmpName"]);

            _strComments = Convert.ToString(dr["Comments"]);
        }

        internal static Comment GetComment(SqlDataReader dr)
        {
            Comment _OneComment = new Comment();
            _OneComment.PopulateItemProperties(dr);
            return _OneComment;
        }
    }
}
