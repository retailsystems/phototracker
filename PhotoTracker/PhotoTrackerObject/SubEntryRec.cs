using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public class SubEntryRec : EntryBase
    {
        public SubEntryRec(WebControl webControl): base(webControl)
        {

        }
         public override void DoNotification(string strMsgType, int nMsgValue, int nEntryType, bool bMsgStatus, int intEmpID, string strItmNum, string strDiv)
         {
            if (m_webControl.GetType() == typeof(TextBox))
            {
                TextBox txtRev = (TextBox)m_webControl;

                if ((strMsgType == "ShowEntry") && (nMsgValue == 1) && (bMsgStatus == true)) //recName
                {
                    txtRev.Enabled = false;
                }

                if ((strMsgType == "SelectShow") && (nMsgValue == 1) && (bMsgStatus == true)) //PerfType = 1 (recev in)
                {
                    txtRev.Text = string.Empty;

                }
            }
           
        }
    }
}
