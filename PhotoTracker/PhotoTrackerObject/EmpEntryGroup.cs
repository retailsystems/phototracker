using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


namespace PhotoTracker
{
    public class EmpEntryGroup : CollectionBase
    {

        public EmpEntryGroup()
        { 
        }

        public void Add(EmpEntry item)
        {
            if (!List.Contains(item))
            {
                List.Add(item);

            }
            else
            {
                throw new IndexOutOfRangeException("object already exists in this list.");
            }
        }

        public static EmpEntryGroup GetEmpEntryDataTable(int intEmpID)
        {
            EmpEntryGroup theEmpEntryGroup = new EmpEntryGroup();
            theEmpEntryGroup.EmpEntryGroupFetch(intEmpID);
            return theEmpEntryGroup;
        }

        private void EmpEntryGroupFetch(int intEmpID)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Emp_GetTrkAccRolesByEmpID";

                SqlDataReader dr;
                cm.Parameters.Clear();

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@EmployeeID";
                mmm.SqlDbType = SqlDbType.Int;
                mmm.Value = intEmpID;
                cm.Parameters.Add(mmm);

                dr = cm.ExecuteReader();

                while (dr.Read())
                    List.Add(EmpEntry.GetEmpEntry(dr));
            }
            //catch (Exception ex)
            //{
            //    //int s=0;

            //}
            finally
            {
                cn.Close();
            }
        }
    }
}
