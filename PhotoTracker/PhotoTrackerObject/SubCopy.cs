using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public class SubCopy : EntryBase
    {
        ListItem itmAll;
        public SubCopy(WebControl webControl) : base(webControl)
        {
            
        }
        public override void DoNotification(string strMsgType, int nMsgValue, int nEntryType, bool bMsgStatus, int intEmpID, string strItmNum, string strDiv)
        {
            if (bMsgStatus == true)
            {
                if (m_webControl.GetType() == typeof(DropDownList))
                {
                    DropDownList dll_webControl = (DropDownList)m_webControl;

                    if ((strMsgType == "SelectShow") && (nMsgValue == 2)) //drop down copy list in;  lMsgValue = 2 =copy
                    {
                        if (nEntryType == 1) //nEntryType =1 = in
                        {
                            DropDownDataBind(dll_webControl, strDiv);
                        }
                    }
                    else if ((strMsgType == "SaveEntry") && (nMsgValue == 2)) //drop down copy list in
                    {
                        if ((nEntryType == 1) && (dll_webControl.SelectedValue != "0"))
                        {
                            //string s = dll_webControl.SelectedValue.ToString();
                            //int m = int.Parse(dll_webControl.SelectedValue.ToString());
                            Data.EntryInsert(strItmNum, int.Parse(dll_webControl.SelectedValue.ToString()),nEntryType, nMsgValue);
                        }
                    }
                   
                }
                else if (m_webControl.GetType() == typeof(TextBox))
                {
                    TextBox txtRev = (TextBox)m_webControl;
                    if ((strMsgType == "SelectShow") && (nMsgValue == 2))
                    {
                        if (nEntryType == 1)
                        {
                            txtRev.Enabled = true;
                        }
                    }
                    else if ((strMsgType == "SaveEntry") && (nMsgValue == 2)) //drop down copy list in
                    {
                        if (nEntryType == 1)  
                        {
                            DateTime dtCopyIn = DateTime.Parse("1/1/0001");
                            
                            if (txtRev.Text != "")
                            {
                                try
                                {
                                    dtCopyIn = DateTime.Parse(txtRev.Text.Trim());
                                    //if (dtCopyIn >= DateTime.Now)
                                    //{
                                        Data.EntryUpdate(strItmNum, nEntryType, DateTime.Parse(txtRev.Text.ToString()), nMsgValue);
                                    //}
                                    //else
                                    //{ Data.EntryUpdate(strItmNum, nEntryType, DateTime.Now, nMsgValue); }
                                }
                                catch
                                {
                                    Data.EntryUpdate(strItmNum, nEntryType, DateTime.Now, nMsgValue);
                                }
                            }
                            else
                            {
                               Data.EntryUpdate(strItmNum, nEntryType, DateTime.Now, nMsgValue);
                            }
                        }
                    }
                }
            }

        }
        public void DropDownDataBind(DropDownList dll_webControl, string strDiv)
        {
            dll_webControl.Items.Clear();
            DataTable dtCopy = Data.GetCopyUsers();  //allow user do cross brand change
            //DataTable dtCopy = Data.GetCopyUsersByDiv(strDiv); 
            dll_webControl.DataSource = dtCopy;

            dll_webControl.DataTextField = "EmpName";
            dll_webControl.DataValueField = "EmployeeID";
       
            dll_webControl.DataBind();
            LoadDefaultOptions(dll_webControl);
            dll_webControl.Enabled = true;
        }
        private void LoadDefaultOptions(DropDownList ddlList)
        {
            itmAll = new ListItem();
            itmAll.Text = "CHOOSE A NAME";
            itmAll.Value = "0";

            ddlList.Items.Insert(0, itmAll);

        }
    }
}
