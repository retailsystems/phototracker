<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FitModel.aspx.cs" Inherits="PhotoTracker.FitModel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FitModel Page</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet"/>
    <script language="javascript" type="text/javascript">
</script>

</head>
<body>
    <form id="form1" runat="server">                                                                                                                                                                                                                                                                                                       
<div id="header"  align="center" style="width: 1003px; height: 263px"><uc1:header id="Header1" runat="server" ></uc1:header>
     <table>
          <tr>
              <td>   <table id="headerTable" style="border-radius: 5px;background-color: #f2f2f2;padding: 20px;" >
          <tr>
              <td>
                    <table class="tableInput" style="border:none;width:550px">                       
                        <tr>
                            <td colspan="2">
                                <strong><span style="font-size: 14pt">Create a New Model Fit </span></strong></td>
                        </tr>                        
                     <%-- <tr>
                            <td style="width: 161px; height: 27px; text-align: right; font-weight: bold;" align="right">
                                Select Model's DivisionID: &nbsp;
                            </td>
                            <td style="width: 101px; height: 28px; text-align: left;">
                             <asp:DropDownList ID="ddlDivisionID" Width="150px" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                                
                                </td>
                        </tr>--%>
                        <tr>
                            <td class="tableCell" >
                                Enter Model's Name: &nbsp;
                            </td>
                            <td class="tableCell">
                                <asp:TextBox ID="txtName"  MaxLength="20" runat="server"></asp:TextBox></td><td style="text-align: left;"> <asp:RequiredFieldValidator ID="RFVName" runat="server" ControlToValidate="txtName"
                        ErrorMessage="Name cannot be blank"  ValidationGroup="CreateModelFit"  ForeColor="#FF3300">*</asp:RequiredFieldValidator></td>
                                
                        </tr> 
                        <tr>
                            <td class="tableCell" >
                                Enter Model's Description: &nbsp;
                            </td>
                            <td class="tableCell">
                                <asp:TextBox ID="txtDescription"  MaxLength="20" runat="server"></asp:TextBox>
                                </td>
                        </tr>   
                        <tr>
                            <td class="tableCell">
                                Enter Model's Height: &nbsp;
                            </td>
                            <td >
                                <asp:TextBox ID="txtModelHeight"  MaxLength="20" runat="server" ></asp:TextBox>
                               </td>
                            <td style="text-align: left;">  <asp:RequiredFieldValidator ID="RFVHeight" runat="server" ControlToValidate="txtModelHeight"
                        ErrorMessage="Height cannot be blank"  ValidationGroup="CreateModelFit" Width="7px" ForeColor="#FF3300">*</asp:RequiredFieldValidator></td>
                        </tr>
                      <%--  <tr>
                            <td class="tableCell">
                                Enter Model's Size: &nbsp;
                            </td>
                            <td class="tableCell">
                                <asp:TextBox ID="txtModelSize"  MaxLength="20" runat="server" ></asp:TextBox>
                               </td>
                            <td style="text-align: left;">  <asp:RequiredFieldValidator ID="RFVSize" runat="server" ControlToValidate="txtModelSize"
                        ErrorMessage="Size cannot be blank."  ValidationGroup="CreateModelFit" Width="7px" ForeColor="#FF3300">*</asp:RequiredFieldValidator></td>
                        </tr>--%>
                         <%-- <tr>
                            <td class="tableCell">
                                Enter Sort Order: &nbsp;
                            </td>
                            <td class="tableCell">
                                <asp:TextBox ID="txtSortOrder"  MaxLength="3" runat="server" ></asp:TextBox>                               
                               </td>
                            <td style="text-align: left;"> <asp:RequiredFieldValidator ID="RFVSortOrder" runat="server" ControlToValidate="txtSortOrder"
                        ErrorMessage="Code cannot be blank"  ValidationGroup="CreateModelFit" Width="7px" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSortOrder" ValidationGroup="CreateModelFit" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        </tr>--%>
                    </table>                 
                   <table class="tableInput" style="border:none;">
                       <tr><td> <asp:Button ID="btnSave" runat="server" Font-Bold="True"  Text="Save" height="27px"
                        Width="100px" OnClick="btnSave_Click" ValidationGroup="CreateModelFit" /></td><td><asp:Button ID="btnAdd" runat="server" Font-Bold="True" height="27px" 
                         Text="NEW"
                        Width="100px" OnClick="btnAdd_Click"  /></td></tr>
                   </table>
                  <asp:CustomValidator ID="valCustom" runat="server" display=None ValidationGroup="CreateModelFit"/>
                         
                    
                </td>
              <td style="text-align:right">
               
              </td>
            </tr>
        </table></td>
              <td><table style="vertical-align:top; width:300px; padding:30px">
         <tr><td style="vertical-align:top"><asp:Label ID="lblCreateModelFitResults"  runat="server"  Font-Size="14pt"  ></asp:Label>
                         <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Error creating Model Fit"
                        ValidationGroup="CreateModelFit" ForeColor="Red" Width="300px"
                         /> </td></tr></table></td>
          </tr>
      </table>
   
     
 
    <br /><br />  

        <asp:HiddenField ID="hdnModelId" runat="server" Value="" />
        <asp:GridView ID="grdModel" runat="server" ShowHeader="true" ShowFooter="false" AutoGenerateColumns="false" OnRowCommand="grdModel_RowCommand" width="1005px" height="141px" style="text-align: center;"  HeaderStyle-BackColor = "#5D7B9D" FooterStyle-BackColor="#5D7B9D"
         Font-Names = "Arial"  Font-Size = "8pt" EnableViewState="true" CssClass="innerTableDetail"  >
                <Columns>
                    <asp:TemplateField HeaderText="Division">
                        <ItemTemplate>
                            <asp:Label ID="lblDivision" runat="server" Text='<%# Eval("Division") %>'></asp:Label>
                              <asp:Label ID="lblModelID" runat="server" Text='<%# Eval("ModelID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Name">
                          <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                          <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                   
                    <asp:TemplateField HeaderText="height">
                      <ItemTemplate>
                            <asp:Label ID="lblHeight" runat="server" Text='<%# Eval("hight") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <%-- <asp:TemplateField HeaderText="Size">
                          <ItemTemplate>
                            <asp:Label ID="lblsize" runat="server" Text='<%# Eval("SIZE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="SortOrder">
                          <ItemTemplate>
                            <asp:Label ID="lblSortOrder" runat="server" Text='<%# Eval("SortOrder") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server"  Text="Edit" CommandName="EditItem" CommandArgument='<%# Eval("ModelID") %>'></asp:LinkButton>/
                             <asp:LinkButton ID="lnkDelete" runat="server"  Text="Delete" CommandName="DeleteItem" CommandArgument='<%# Eval("ModelID") %>' OnClientClick="return confirm('Are you sure want to delete?');"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 
            </asp:GridView>
   
    </div>
      </form>
</body>
</html>

