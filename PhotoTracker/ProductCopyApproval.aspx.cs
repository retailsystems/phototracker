using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;
using System.Text;
using FreeTextBoxControls;



namespace PhotoTracker
{
    public partial class ProductCopyApproval : System.Web.UI.Page
    {
        protected Item theGersTrkItem;
        protected ItemEntry theItemEntryInfo;
        protected ItemEntryGroup theItemEntryGroup;
        protected EmpEntryGroup empEntryGroup;
        protected CommentGroup newCommentGroup;
        public DataTable theItemTopInfo;
        public DataTable dtUser;
        public DataTable dtPermit;
        public string strItemNum = string.Empty;
        public int EmployeeID = 0;
        public string strDiv = string.Empty;
        public string strBarCodeDir = Data.strBarCodePath();
        public string strItmNum = string.Empty;
        public string strZoomedParams = string.Empty;
        public bool bShowImg = false;
        string strPath = ConfigurationManager.AppSettings["strImgDirPath"];
        public string strTopSamp = string.Empty;

        public string strItmPrimaryImg = string.Empty;
        string strImgHTurl= ConfigurationManager.AppSettings["strImgHTurl"];        
        public string errString;
        public string rmsItemNum = string.Empty;
        public string rmsItemDesc = string.Empty;
        public string catalogDesc = string.Empty;
        public string dptNum = string.Empty;
        public string clsNum = string.Empty;
        public string subclsNum = string.Empty;
        public string phTitle = string.Empty;
        public string phDtlDesc = string.Empty;
        public string dtlDescTxt = string.Empty;
        public string primaryIMGPath = string.Empty;
        public string lastUpdateId = string.Empty; 
        public string lastUpdateDate = string.Empty;
        public string lastUpdateDtl = string.Empty;
        public string lastApprDate ;
        public string statusCode = string.Empty;
        SqlDataAdapter da1 = new SqlDataAdapter();
        DataSet ds1 = new DataSet();
        SqlCommand _cmd = new SqlCommand();
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();

               
        public ArrayList listImages = new ArrayList();
        public ArrayList listImgDBSave = new ArrayList();
        public ArrayList listAltImgDBSave = new ArrayList();
         
        public void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                strZoomedParams = "?$400x600$"; //"?$35x53$";
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];
   

                //ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {

                    txtItemNum.Visible = true;
                    searchbBtn.Visible = true;
                    BarcodeWebImage2.Visible = false;
                    last_upd_dt_id.Enabled = false;
                    itemNum.Enabled = false;
                    catDesc.Enabled = false;
                    txtRMSDesc.Enabled = false;
                    deptNum.Enabled = false;
                    classNum.Enabled = false;
                    subClassNum.Enabled = false;
                    Image52.Visible = false;
                    itemDtlDesc.Visible = false;
                    itemDtlDesc.Enabled = false;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;
                    itemTitle.Enabled = false;
                    //itemTitle.Enabled = false;
                    //save.Enabled = false;
                    //approve.Enabled = false;
                    unApprove.Enabled = false;
                    //searchbBtn.Enabled = false;
                    ////imgItem.Visible = false;
                    dtlDescEdit.Visible = false;
                    titleEdit.Visible = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }

        }


        private bool ImageExists(string imgURL)
        {
            WebRequest webRequest = WebRequest.Create(imgURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }
        
        
        private void ClearExpItmTxt()
        {
            itemNum.Visible = false;
            catDesc.Visible = false;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Visible = false;
            last_upd_dt_id.Visible = false;
            apprDate.Visible = false;
            itemTitle.Visible = false;
            itemDtlDesc.Visible = false;
            deptNum.Visible = false;
        }

        private void EmtyExpItmTxt()
        {
            itemNum.Text = string.Empty;
            catDesc.Text = string.Empty;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Text = string.Empty;
            last_upd_dt_id.Text = string.Empty;            
            apprDate.Text = string.Empty;
            itemTitle.Text = string.Empty;
            itemDtlDesc.Text = string.Empty;
            deptNum.Text = string.Empty;
            classNum.Text = string.Empty;
            imgPath.Text = string.Empty;
            subClassNum.Text = string.Empty;
            FreeTextBox1.Text = string.Empty;
            FreeTextBox1.EnableHtmlMode = false;
            FreeTextBox1.EnableViewState = false;
            FreeTextBox1.EnableToolbars = false;
            FreeTextBox1.EnableSsl = false;
            FreeTextBox1.ReadOnly = true;
        }


        private void EnableTxt(object sender, EventArgs e)
        {
            itemNum.Visible = true;
            catDesc.Visible = true;
            BarcodeWebImage2.Visible = true;
            txtRMSDesc.Visible = true;
            last_upd_dt_id.Visible = true;
            apprDate.Visible = true;
            itemTitle.Visible = true;
            //itemDtlDesc.Visible = true;
            deptNum.Visible = true;
            classNum.Visible = true;
            skuGrdView.Visible = true;
            imgPath.Visible = true;
            subClassNum.Visible = true;

        }

        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            ClearExpItmTxt();
        }


        private void LoadEntryTime(TextBox txtTimeBox, string strTime)
        {
            txtTimeBox.Text = string.Empty;
            txtTimeBox.Text = strTime;

            if (strTime != "")
            {
                txtTimeBox.Enabled = false;

            }
        }
        private void LoadRecvName(TextBox txtRecNameBox, string strName)
        {
            txtRecNameBox.Text = string.Empty;
            txtRecNameBox.Text = strName;

            if (strName != "")
            {
                txtRecNameBox.Enabled = false;

            }
        }
        private void LoadDropdownName(DropDownList ddlList, string strName, bool IsSelected)
        {
            ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            if (IsSelected)
            {
                ddlList.Enabled = false;

            }
        }
        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }

        
        
        
        protected int GetItemDtl(string strItemNum, ref string strErrorMsg)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;            

            int returnCode = 0;

            strErrorMsg = string.Empty;

            try
            {

                conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                conn.Open();
                cmd = new SqlCommand("SELECT im.Style,im.description rmsItmDesc, ci.description catDesc, " +
                                     "im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, im.lastphupdateid, " +
                                     " im.lastphupdatedate,im.lastphapprdate,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc FROM [RMS_PH_STYLE_Master] im " +
                                     " INNER JOIN [Catalog_Id] ci ON im.division = ci.division where " +
                                     "  (im.Style='" + strItemNum + "' or  im.Style in (select Program from [RMS_DW_ITEM_STG] im1 " + //need to change the sku table name
                                     " where im1.sku='" + strItemNum + "')) ", conn); 

                SqlDataReader reader = cmd.ExecuteReader();
                
                //conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                //conn.Open();
                /*cmd = new SqlCommand("SELECT im.itemnum,im.description rmsItmDesc, [Catalog_Id].description catDesc FROM [RMS_Item_Master] im " +
                                     " INNER JOIN [Catalog_Id] ON im.division = [Catalog_Id].division where im.Program is null " +
                                     " and (im.itemnum='" + strItemNum + "' or  im.itemnum in (select Program from [RMS_Item_Master] im1 " +
                                     " where im1.itemnum='" + strItemNum + "')) ", conn); */

                //reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    /*if (reader["Style"].ToString() != null && reader["Style"].ToString() != string.Empty)
                    {
                        rmsItemNum = reader["Style"].ToString();
                    }
                    if (reader["rmsItmDesc"].ToString() != null && reader["rmsItmDesc"].ToString() != string.Empty)
                    {
                        rmsItemDesc = reader["rmsItmDesc"].ToString();
                    }
                    if (reader["catDesc"].ToString() != null && reader["catDesc"].ToString() != string.Empty)
                    {
                        catalogDesc = reader["catDesc"].ToString();
                    }*/
                    rmsItemNum     = reader["Style"].ToString();
                    rmsItemDesc    = reader["rmsItmDesc"].ToString();
                    catalogDesc    = reader["catDesc"].ToString();
                    dptNum         = reader["department"].ToString();
                    clsNum         = reader["class"].ToString();
                    subclsNum      = reader["subclass"].ToString();
                    phTitle        = reader["phtitle"].ToString();
                    primaryIMGPath = reader["primaryimgpath"].ToString();
                    phDtlDesc = reader["dtlDesc"].ToString();
                    if (reader["lastphupdatedate"].ToString() != null && reader["lastphupdatedate"].ToString() != string.Empty)
                    {
                        //lastUpdateDate = FilterDate(Convert.ToDateTime(reader["lastphupdatedate"].ToString()));
                        lastUpdateDate = reader["lastphupdatedate"].ToString();
                    }
                    if (reader["lastphupdateid"].ToString() != null && reader["lastphupdateid"].ToString() != string.Empty)
                    {
                        lastUpdateId = reader["lastphupdateid"].ToString();
                    }
                    if ((lastUpdateDate == null || lastUpdateDate == string.Empty) && (lastUpdateDtl == null || lastUpdateDtl == string.Empty))
                    {
                        lastUpdateDtl = string.Empty;
                    }
                    else
                    {
                        lastUpdateDtl = "[" + lastUpdateId + "," + lastUpdateDate + "]";
                    }

                    if (reader["lastphapprdate"].ToString() != null && reader["lastphapprdate"].ToString() != string.Empty)
                    {
                        lastApprDate = FilterDate(Convert.ToDateTime(reader["lastphapprdate"].ToString()));
                    }

                    statusCode = reader["status"].ToString();
                    dtlDescTxt = reader["LongDesc"].ToString();

                }

                cmd.Dispose();
                conn.Close();

                returnCode = 0;
            }
            catch (Exception ex)
            {
                strErrorMsg = ex.Message;
                System.Diagnostics.EventLog.WriteEntry("Photocopyapproval", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                //LogError("An error occurred in GetItemDtl function while fetching the item details " + ex.ToString());
                returnCode = -1;
            }

            return returnCode;
        }

        //Deba
        protected void searchBtnClicked(object sender, EventArgs e)
        {
            bool bItem = false;
            //lblNoItemMsg.Text = string.Empty;
            errString = string.Empty;
            int intRevResult = 0;
            int intItemExixt = 0;
            int intRetCode = 0;

            //ClearExpItmTxt();
            lblNoItemMsg.Text=string.Empty;
            EmtyExpItmTxt();

            strItemNum = this.txtItemNum.Text.Trim();

            if (strItemNum != "")
            {
                ViewState["StrItemNum"] = strItemNum;

                intRetCode = GetItemDtl(strItemNum, ref errString);

                if (intRetCode == 0)
                {
                    itemNum.Text             = rmsItemNum;
                    txtRMSDesc.Text          = rmsItemDesc;
                    catDesc.Text             = catalogDesc;
                    BarcodeWebImage2.Visible = true;
                    BarcodeWebImage2.Value = rmsItemNum;
                    imgPath.Text    = primaryIMGPath;
                    last_upd_dt_id.Text = lastUpdateDtl;
                    apprDate.Text = lastApprDate;
                    itemTitle.Text = phTitle;
                    //itemDtlDesc.Text=phDtlDesc;
                    FreeTextBox1.Text = dtlDescTxt;
                    itemDtlDesc.Text = dtlDescTxt;
                    FreeTextBox1.ReadOnly = true;
                    deptNum.Text = dptNum;
                    classNum.Text = clsNum;
                    subClassNum.Text = subclsNum;
                    dtlDescEdit.Visible = true;
                    titleEdit.Visible = true;
                    skuBindData(rmsItemNum);
                    skuGrdView.Visible = true;
                    //
                    if (primaryIMGPath != null && primaryIMGPath != string.Empty)
                    {
                        strItmPrimaryImg = strImgHTurl + primaryIMGPath;
                    }
                    ShowItmImage(rmsItemNum);

                    if (statusCode == "APPROVED")
                    {
                        unApprove.Enabled = true;
                        approve.Enabled = false;
                    }
                    else if ((statusCode == "SAVED" || statusCode == "UNAPPROVED") && (FreeTextBox1.Text != string.Empty && itemTitle.Text!=string.Empty))
                    {
                        approve.Enabled = true;
                    }
                }
                else
                {
                    lblNoItemMsg.Text = "Item # " + strItemNum + " does not exist. Please re-enter.";
                    //ClearAll();
                    EmtyExpItmTxt();
                }
                
            }
            else
            {
                ClearAll();
                //pnlRecv.Visible = false;
                //pnlUpdate.Visible = true;
                //pnlEntry.Visible = false;
                lblNoItemMsg.Text = "You must input Item Number";
            }
        }
        //Deba

      
        private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            strZoomedParams = "?$400x600$";
            strItemImage = ConfigurationManager.AppSettings["strItemImage"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit"];
            imgDBItem = imgDB.Replace("[picvalue]", strItemNum);
 
            if (strItmPrimaryImg != string.Empty)
            {
                strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL;
                }
            }
                       
            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                listImages.Add("_hi");
                listImgDBSave.Add(imgDBItem+"_hi");

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                        listImgDBSave.Add(imgDBItem + strSml);
                    }
                }
                bShowImg = true;

                /*if (!listImgDBSave.Contains(primaryIMGPath.Replace("/is/image/HotTopic/","")))
                {
                    listImages.Add();
                }*/
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }
      
        private void skuBindData(string strRMSItemNum)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String skuSql=string.Empty;
            try
            {
                Sqlconn.Open();
                skuSql = "select sku skuNum,size sizeId,color colorId,ats ats,unit_retail price from [RMS_DW_ITEM_MASTER] where program='" + strRMSItemNum + "' order by sku";

                da1.SelectCommand = new SqlCommand(skuSql, Sqlconn);
                da1.Fill(ds1, "tblSkuDtl");
                skuGrdView.DataSource = ds1.Tables["tblSkuDtl"];
                skuGrdView.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }

        protected void titleEdit_Click(object sender, EventArgs e)
        {
            /*if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                strZoomedParams = "?$215x219$"; //"?$35x53$";
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];


                //ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {*/
                    itemTitle.Enabled = true;
                    /*}
                    else
                    {
                        lblNoItemMsg.Text = "You need Edit permission to do the changes";
                    }
                }*/

        }


        protected void titleEdit_Click1(object sender, EventArgs e)
        {
            itemTitle.Enabled = true;
            if (strItmPrimaryImg == string.Empty)
            {
                if (this.imgPath.Text != null && this.imgPath.Text != string.Empty)
                {
                    strItmPrimaryImg = strImgHTurl + this.imgPath.Text;
                }
                ShowItmImage(this.itemNum.Text);
            }
        }

        protected void dtlDescEdit_Click(object sender, EventArgs e)
        {
            //itemDtlDesc.Enabled = true;
            FreeTextBox1.EnableHtmlMode = true;
            FreeTextBox1.EnableViewState = true;
            FreeTextBox1.EnableToolbars = true;
            FreeTextBox1.EnableSsl = true;
            FreeTextBox1.ReadOnly = false;
            FreeTextBox1.Text = itemDtlDesc.Text;
            itemDtlDesc.Text = string.Empty;
            if (strItmPrimaryImg == string.Empty)
            {
                if (this.imgPath.Text != null && this.imgPath.Text != string.Empty)
                {
                    strItmPrimaryImg = strImgHTurl + this.imgPath.Text;
                }
                ShowItmImage(this.itemNum.Text);
            }

        }

        protected string get_UserID()
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
            string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
            string userId = strFirstName.Substring(0, 1) + strLastName;

            return userId;
        }

        protected void save_Click(object sender, EventArgs e)
        {

            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;

            //String tempFreeText = this;
            //String tempFreeText = FreeTextBox1.

            String lastUpdId = apprDate.Text;
            String tempText1 = FreeTextBox1.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId=get_UserID();

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }



            /*String primIMG = string.Empty;

            foreach (string tmp in listImgDBSave)
            {
                if (this.imgPath.Text!=string.Empty)
                {
                    primIMG=this.imgPath.Text;
                }
                else if (tmp.Contains("_hi"))
                {
                    primIMG=tmp;
                }

                if (primIMG != string.Empty)
                {
                    if (primIMG != tmp)
                    {
                        listAltImgDBSave.Add(tmp);
                    }
                }
            }*/
            
             try
            {
            //EnableTxt();
                if ((newItmTitle == null || newItmTitle == string.Empty) && (tempText1 == null || tempText1 == string.Empty) && (this.imgPath.Text == null || this.imgPath.Text == string.Empty))
                {
                    //EnableTxt();
                    lblNoItemMsg.Text = "Item Enrichment can't be saved without Title , Detail description or Primary Image Path";
                    //pnlButton.Visible = true;
                }

                else
                {
                    String statusSql = string.Empty;
                    byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                    //update the status and Title
                    /*statusSql = "update RMS_PH_STYLE_Master set status='SAVED',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg,"+
                        "dtlDesc=@dtlDesctxt,temp_dtl_txt=@dtl_text" +
                        " where style='" + this.itemNum.Text + "'";*/

                    statusSql = "update RMS_PH_STYLE_Master set status='SAVED',publishedInd='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg," +
                        "dtl_Desc_txt=@dtldesctext" +
                        " where style='" + this.itemNum.Text + "'";
                    Sqlconn.Open();
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                    _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    /*if (lastApprDT == null || lastApprDT == string.Empty)
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now );
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                    }*/
                    _cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                    _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", string.Empty);
                    //_cmd.Parameters.AddWithValue("@dtl_text", dtlDesctxt);                    
                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();

                    lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "saved succssfully";
                    EmtyExpItmTxt();
                    txtItemNum.Text = string.Empty;
                    skuGrdView.Visible = false;
                    
                }

                   

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender,e);
            //ShowItmImage(this.itemNum.Text);
            /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
            {
                approve.Enabled = true;
            }*/

        }

        protected void approve_Click(object sender, EventArgs e)
        {
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;

            String lastUpdId = apprDate.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }

            try
            {
                //EnableTxt();
                if ((newItmTitle == null || newItmTitle == string.Empty) || (tempText1 == null || tempText1 == string.Empty))
                {
                    //EnableTxt();
                    lblNoItemMsg.Text = "Item Enrichment can't be approved without Title and Detail description";
                    ShowItmImage(this.itemNum.Text);
                    //pnlButton.Visible = true;
                }

                else
                {
                    String statusSql = string.Empty;
                    byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                    //update the status and Title
                    statusSql = "update RMS_PH_STYLE_Master set status='APPROVED',publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprID=@lastApprID" +
                        " where style='" + this.itemNum.Text + "'";

                    Sqlconn.Open();
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                    _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    if (lastApprDT == null || lastApprDT == string.Empty)
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now);
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                    }
                    _cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                    _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                    _cmd.Parameters.AddWithValue("@lastApprID", lastUpdId);

                    _cmd.CommandType = CommandType.Text;
                    try
                    {
                        _cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                    finally
                    {
                        Sqlconn.Close();
                        Sqlconn.Dispose();
                    }

                    lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "approved succssfully";
                    EmtyExpItmTxt();
                    txtItemNum.Text = string.Empty;
                    skuGrdView.Visible = false;
                }
                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
            //ShowItmImage(this.itemNum.Text);
        }

        protected void unApprove_Click(object sender, EventArgs e)
        {
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;

            String lastUpdId = apprDate.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }


            try
            {

                String statusSql = string.Empty;
                byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                //update the status and Title
                statusSql = "update RMS_PH_STYLE_Master set status='UNAPPROVED',publishedInd='N',Online_Flag='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprDate=@lastApprDT,LastPHApprID=@lastApprID" +
                    " where style='" + this.itemNum.Text + "'";

                Sqlconn.Open();
                _cmd = new SqlCommand(statusSql, Sqlconn);
                _cmd.Parameters.AddWithValue("@Title", this.itemTitle.Text);
                _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                _cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                //_cmd.Parameters.AddWithValue("@dtlDesctxt", this.itemDtlDesc.Text);
                _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                _cmd.Parameters.AddWithValue("@lastApprDT", string.Empty);
                _cmd.Parameters.AddWithValue("@lastApprID", string.Empty);
                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();

                lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "unapproved succssfully";
                EmtyExpItmTxt();
                txtItemNum.Text = string.Empty;
                skuGrdView.Visible = false;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
                //ShowItmImage(this.itemNum.Text);
                /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
                {
                    approve.Enabled = true;
                }*/
                

        }

        protected void textChanged1(object sender, EventArgs e)
        {
            if (itemTitle.Text != null && itemTitle.Text != string.Empty )
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }
        protected void textChanged2(object sender, EventArgs e)
        {
            if (imgPath.Text != null && imgPath.Text != string.Empty)
            {
                save.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }
        protected void textChanged3(object sender, EventArgs e)
        {
            if (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty)
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }

        /*protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {
            if (txtItemNum.Text != null && txtItemNum.Text != string.Empty)
            {
                searchbBtn.Enabled = true;
            }
            else
            {
                searchbBtn.Enabled = false;
            }
            ShowItmImage(this.itemNum.Text);
        }*/

    }
}
