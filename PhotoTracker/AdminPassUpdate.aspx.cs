using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace PhotoTracker
{
    public partial class AdminPassUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
            //_Sqlconn = new SqlConnection("Data Source=casql05dev;User ID=phototracker_user; Password=photo$456; Initial Catalog=PhotoTracker");
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            SqlCommand _cmd = new SqlCommand();
            SqlDataAdapter SQLDataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlDataReader _rdr = null;
            try
            {
                _Sqlconn.Open();

                string sql = "SELECT EmployeeID FROM  PhotoTracker.dbo.Employee WHERE EmployeeID = "+txtEmpID.Text;
                SQLDataAdapter = new SqlDataAdapter(sql, _Sqlconn);                
                    SQLDataAdapter.Fill(ds, "tblEmployee");


                    if (ds.Tables["tblEmployee"].Rows.Count != 0)
                    {
                        //Set employee password


                        _cmd = new SqlCommand("SetEmpPWD", _Sqlconn);
                        _cmd.CommandType = CommandType.StoredProcedure;

                        _cmd.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Int, 6, "EmpID"));
                        _cmd.Parameters.Add(new SqlParameter("@NewPWD", SqlDbType.VarChar, 25, "NewPwd"));
                        _cmd.Parameters.Add(new SqlParameter("@Result", SqlDbType.Int, 1, "Result"));
                        _cmd.Parameters["@Result"].Direction = ParameterDirection.Output;

                        _cmd.Parameters[0].Value = txtEmpID.Text;
                        _cmd.Parameters[1].Value = txtNewPwd.Text;


                        // execute the command
                        _rdr = _cmd.ExecuteReader();



                        int result = Int32.Parse(_cmd.Parameters["@Result"].Value.ToString());


                        if (result == 1)
                        {

                            lblPwdChangeResult.Text = "Password has been changed successfully for EmployeeID: "+txtEmpID.Text;
                            //lblPwdChangeResult.Text = "Successfully setup password for EmployeeID: " + txtEmpID.Text + "-" + result;

                        }
                        else
                        {

                            lblPwdChangeResult.Text = "Unable to change password for EmployeeID: " + txtEmpID.Text + ".  Please contact IT." + result;
                            //lblPwdChangeResult.Text = "Unable to setup password for EmployeeID: "+txtEmpID.Text+".  Please contact IT." + result;
                        }

                    }

                    else
                    {
                        lblPwdChangeResult.Text = "EmployeeID: " + txtEmpID.Text + "  does not exists in PhotoTrack system";
                    }


                
                //_rdr.Close();
                _Sqlconn.Close();


            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {

            Response.Redirect("TrackMenu.aspx");


        }

        protected void Header1_Load(object sender, EventArgs e)
        {

        }
    }
}