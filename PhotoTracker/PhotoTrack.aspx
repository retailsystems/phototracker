<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="PhotoTrack.aspx.cs" Inherits="PhotoTracker.PhotoTrack" %>
<%@ Register Assembly="Bytescout.BarCode" Namespace="Bytescout.BarCode" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<HTML>
	<HEAD>
		<title>PhotoTrack</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            
            function Comments()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('Comments.aspx?strItmNum=' + itemnum, 'Comments', 'width=600,height=600,resizable=yes');
            }
		</script>
	</HEAD>
		<body onload="javascript:document.Form1.txtItemNum.focus();" bottomMargin="0" bgColor="#000000" topMargin="0" >
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header></div>
			<div id="main" style="width: 988px">
				<TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="96%"
					align="center" border="0" runat="server">
					<TBODY>
						<tr>
							<td class="innerTableDetail" colSpan="2" style="height: 183px">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0" style="height: 147px">
									<TBODY>
										<tr>
											<td colSpan="2" style="height: 81px; width: 363px;">
												<table style="WIDTH: 500px; height: 60px;" width="100%" border="0">
													<TR>
														<TD class="tableTitle" colSpan="2" valign="middle">Items Details</TD>
													</TR>
													<TR>
														<td>
															<table style="WIDTH: 500px;" border="0">
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Item Input: </td>
																</TR>
																<TR><TD style="HEIGHT: 21px"><table><tr style="HEIGHT: 21px"><td width="150"><asp:textbox id="txtItemNum" runat="server" CssClass="tableInput" columns="12" OnTextChanged="txtItemNum_TextChanged"></asp:textbox></td>
																    <td width="350"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td></tr></table></TD>
																	
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" width="126">Description:</TD>
																</TR>
																<TR><TD style="HEIGHT: 21px"><asp:textbox ReadOnly ="True" id="txtDescription" runat="server" CssClass="tableInput" columns="87"></asp:textbox></TD>
																</TR>
																
															</table>
														</td>
													</TR>
													<tr><td style="HEIGHT: 6px"><table border="0">
													            <tr><td style="HEIGHT: 6px" width="220">IN BY</td><td style="HEIGHT: 6px; width: 20px;"></td><td>SAMPLE IN</td></tr>
													            <tr><td style="HEIGHT: 8px" width="220"><asp:textbox id="txtReceivIn" runat="server" CssClass="tableInput" columns="33"></asp:textbox></td><td style="HEIGHT: 6px; width: 20px;"></td><td><asp:textbox id="txtSampleDateIn" runat="server" CssClass="tableInput" columns="18"></asp:textbox></td></tr>
													</table></td></tr>
												     
												</table>
                                               
											</td>
											<td style="height: 81px; width: 400px;" valign ="bottom">
												<table cellSpacing="0" cellPadding="0" width="50%" border="0">
													<TR>
																	
																	<TD style="HEIGHT: 21px">&nbsp;
																		<asp:radiobuttonlist id="rdTopSam" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rdTopSam_SelectedIndexChanged" ToolTip="Please select TOP or SAMPLE item type.">
																		<asp:listitem value="TOPS">TOPS</asp:listitem>
																	    <asp:listitem value="SAMPLE">SAMPLE</asp:listitem></asp:radiobuttonlist>
																	</TD>
																	<td></td>
																	</TR>
												</table>
											</td>
										</tr>
									</TBODY>
								</TABLE>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" vAlign="top" style="height: 543px">
								<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
									<TR>
										<TD class="tableTitle" style="HEIGHT: 33px" colSpan="3">Entry &nbsp;Information</TD>
									</TR>
									<TR height="10">
										<td align="right"><font class="fontSmallBold">( Format: MM/DD/YYYY )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td></TR>

						            <tr><td style="width: 516px" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%" style="height: 50px"><b>&nbsp; Copy: </b></TD>
										<td class="tableCell" style="height: 50px">
										  <table style="height: 25px" border="0" cellSpacing="0" cellPadding="0">
										      <tr height="25"><td valign="bottom">IN</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlCopyIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlCopyIn_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtCopyDateIn" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtCopyDateIn');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
									         <asp:CustomValidator id="cvalCopyIn" runat="server" ErrorMessage="Must specify valid Copy In date"
									Display="Dynamic" ControlToValidate="txtCopyDateIn"></asp:CustomValidator></TD>
									         </TR>
									         
									         <tr height="25"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlCopyOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlCopyOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom">
						                          <asp:textbox id="txtCopyDateOut" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtCopyDateOut');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A><asp:CustomValidator id="cvalCopyOut" runat="server" ErrorMessage="Must specify valid Copy Out date" Display="Dynamic"
									ControlToValidate="txtCopyDateOut"></asp:CustomValidator>
									          </TD>
									         </TR>
									         <tr height="10"><td></td></tr>
									         </table>
                                           	<asp:CustomValidator id="cvalCopyDateRange" runat="server" ErrorMessage="Copy In Date must be earlier than Out Date!"
									Display="Dynamic" ControlToValidate="txtCopyDateOut"></asp:CustomValidator></td>
									 </TR>
									</table></td></tr>  
									
									<tr><td style="width: 516px" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>&nbsp; Photo: </b></TD>
										<td class="tableCell">
										  <table style="height: 25px" border="0" cellSpacing="0" cellPadding="0">
										      <tr height="25"><td valign="bottom">IN</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlPhotoIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhotoIn_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtPhotoDateIn" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhotoDateIn');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="25"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlPhotoOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhotoOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%">
						                          <asp:textbox id="txtPhotoDateOut" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhotoDateOut');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         <tr height="10"><td></td></tr>
									         </table>
									      </td>
									 </TR>
									</table></td></tr>  
									<tr><td style="width: 516px;" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>
										 &nbsp; Photo&nbsp;<br /> &nbsp; Edit:</b></TD>
										<td class="tableCell">
										  <table style="height: 25px" border="0" cellSpacing="0" cellPadding="0">
										      <tr height="25"><td valign="bottom">IN</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlPhEditIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhEditIn_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtPhEditIn" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditIn');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="25"><td valign="bottom" style="height: 25px">OUT</td>
     						                   <TD valign="bottom" width="48%" style="height: 25px">
     						                         <asp:dropdownlist id="ddlPhEditOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlPhEditOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%">
						                          <asp:textbox id="txtPhEditOut" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditOut');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         <tr height="10"><td></td></tr>
									         </table>
									      </td>
									 </TR>
									</table></td></tr>
									<tr><td style="width: 516px" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>&nbsp; Final <br /> &nbsp;&nbsp;App: </b></TD>
										<td class="tableCell">
										  <table style="height: 25px" border="0" cellSpacing="0" cellPadding="0">
										      <tr height="25"><td valign="bottom">IN</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlFinalIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlFinalIn_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtFinalDateIn" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtFinalDateIn');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
									         </TD>
									         </TR>
									         
									         <tr height="25"><td valign="bottom" style="height: 25px">OUT</td>
     						                   <TD valign="bottom" width="48%" style="height: 25px">
     						                         <asp:dropdownlist id="ddlFinalOut" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" OnSelectedIndexChanged="ddlFinalOut_SelectedIndexChanged"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%" style="height: 25px">
						                          <asp:textbox id="txtFinalDateOut" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.ddlFinalOut');"
								href="javascript:;"><IMG height="25" alt="cal" hspace="3" src="Image/calendar.jpg" width="30" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         </table>
									      </td>
									 </TR>
									
									</table></td></tr>
									  <tr height="52" valign="bottom"><td>
									  <table><tr><td width = "250" valign="bottom">
									         <table><tr height = "82"><td valign="bottom">&nbsp<a href="javascript:Comments()"><IMG src="/Image/Comments.gif" border="0"></a></td></tr>
									                <tr><td width = "250" valign="bottom"><font color="#cc0000"><asp:Label ID ="lblComExist" runat="server"></asp:Label></font></td></tr>
									         </table></td>
									             <td><cc1:barcodewebimage id="BarcodeWebImage2" runat="server" RenderingHint="SystemDefault" SmoothingMode="Default" Value="DATA" ForeColor="Black" HorizontalAlignment="Right" Angle="Degrees0" CaptionPosition="Below" NarrowBarWidth="3" VerticalAlignment="Top" AddChecksum="False" AddChecksumToCaption="False" AdditionalCaptionPosition="Above" AdditionalCaptionFont="Arial, 13pt" BackColor="White" BarHeight="50" CaptionFont="Arial, 13pt" DrawCaption="True" WideToNarrowRatio="3" Symbology="Code128" AdditionalCaption=" " EnableTheming="True"></cc1:barcodewebimage></td>
									  </tr></table>
									  </td></tr>      
								</TABLE>
							</td>
							<TD class="innerTableDetail" vAlign="top" width="40%" align="center" style="height: 543px">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0" style="height: 513px">
									<TBODY>
										<TR>
											<TD class="tableTitle" style="HEIGHT: 49px" colSpan="2">Item&nbsp;Information</TD>
										</TR>
										<TR valign="top">
											<td style="height: 341px" class="innerTableDetailThin" align="center">
												<table style="WIDTH: 220px; HEIGHT: 48px" border="0" align="center">
													<TR width="67">
														<TD>&nbsp;&nbsp;&nbsp;&nbsp;DIVISION:</TD>
													</TR>
													<TR>
												         <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtDivision" runat="server" CssClass="tableInput" columns="23"></asp:textbox></TD>
													</TR>
													<TR>
														<TD>&nbsp;&nbsp;&nbsp;&nbsp;DEPARTMENT:</TD>
													</TR>
													<TR>
												         <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtDept" runat="server" CssClass="tableInput" columns="23"></asp:textbox></TD>
													</TR>
													<TR>
														<TD>&nbsp;&nbsp;&nbsp;&nbsp;CLASS:</TD>
													</TR>
													<TR>
												         <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtClass" runat="server" CssClass="tableInput" columns="23"></asp:textbox></TD>
													</TR>
													<TR>
														<TD>&nbsp;&nbsp;&nbsp;&nbsp;SUBCLASS:</TD>
													</TR>
													<TR>
												         <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtSubclass" runat="server" CssClass="tableInput" columns="23"></asp:textbox></TD>
													</TR>
													<TR>
														<TD>&nbsp;&nbsp;&nbsp;&nbsp;PRICE:</TD>
													</TR>
													<TR>
												         <TD>&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtPrice" runat="server" CssClass="tableInput" columns="23"></asp:textbox></TD>
													</TR>
													<tr><td style="height: 3px"></td></tr>
													<TR><td style="height: 210px" class="innerTableDetailThin">
													   <table style="width: 210px; height: 210px"><tr>
												         <TD style="height: 210px" width="120" ><asp:Image id="imgItem" runat="server"></asp:Image></TD>
													   </tr></table>
													</td></TR>
												</table>
											</td><td style="height: 341px" width="230"><table border="0" class="innerTableDetailThin">
                                                     <TR>
														<TD HEIGHT="22" style="width: 156px"></TD>
													</TR>
                                                    <TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;CANCEL DATE</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtCancelDate" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 1</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme1" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 2</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme2" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="20" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 3</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme3" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 4</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme4" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 5</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme5" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="22" style="width: 156px">&nbsp;&nbsp;&nbsp;PRIV. LABEL</TD>
													</TR>
													<TR>
												         <TD style="width: 156px; height: 26px;">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtPriLabel" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
												         <TD style="width: 156px; height: 26px;"></TD>
													</TR>
													<TR>
												         <TD style="width: 156px; height: 26px;"></TD>
													</TR>
													<TR>
												         <TD style="width: 156px; height: 26px;"></TD>
													</TR><TR>
												         <TD style="width: 156px; height: 26px;"></TD>
													</TR>
													</table></td>
										</tr>
									   <tr><td><asp:panel runat=server id=pnlUpdate visible=true><table><tr><td><asp:Button id="btnUpdate" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnUpdate_Click"></asp:Button></td></tr></table></asp:panel>
									                    <asp:panel runat=server id=pnlRecv visible=false><table><tr><td><asp:Button id="btnRecv" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnRecv_Click"></asp:Button></td></tr></table></asp:panel>
									                    <asp:panel runat=server id=pnlEntry visible=false><table><tr><td><asp:Button id="btnEntry" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnEntry_Click"></asp:Button></td></tr></table></asp:panel>
									                    </td>
									                    <td>
															<asp:Button id="btnClear" runat="server" Text="CLEAR" CssClass="buttonRed" OnClick="btnClear_Click"></asp:Button></td></tr>
									</TBODY>
									
								</TABLE>
								
							</TD>
							
						</tr>
						
					</TBODY>
				</TABLE>
				
			</div>
			

		</form>
	</body>
</HTML>
