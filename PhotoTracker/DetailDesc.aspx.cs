using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FreeTextBoxControls;

namespace PhotoTracker
{
    public partial class DetailDesc : System.Web.UI.Page
    {
        /*protected CommentGroup newCommentGroup;
        protected string strItemNum = string.Empty;
        public DataTable dtUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            strItemNum = Request.QueryString["strItmNum"];
            ViewState["StrItemNum"] = strItemNum;

            LoadComments(strItemNum);
 
        }

        protected void dlItemView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Write("<script language='javascript'> { self.close() }</script>");
        }

        protected void btnSaveNotes_Click(object sender, EventArgs e)
        {
            string strItemNum = string.Empty;
            int nComResult = 0;
            int intEmployeeID = 0;
            strItemNum = ViewState["StrItemNum"].ToString();
            if (strItemNum != "")
            {
                dtUser = (DataTable)Session["UserProfile"];
                if (dtUser != null)
                {
                    intEmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());

                    nComResult = Data.CommentInsert(strItemNum, intEmployeeID, txtComments.Text.ToString());

                    if (nComResult == 1)
                    {
                        txtComments.Text = string.Empty;
                        LoadComments(strItemNum);
                    }
                    else
                    {
                        //insert failed
                        lblNoItemMsg.Text = "Could not save Item # " + strItemNum + " to Database";
                    }
                }
                else
                {
                    //could not find empID
                    lblNoItemMsg.Text = "Could not find empployee name for Item # " + strItemNum + " on Database.";
                }
            }
            else
            {
                lblNoItemMsg.Text = "Please input an item number on PhotoTracking.aspx page first.";
            }
        }

        private void LoadComments(string strItemNum)
        {
            newCommentGroup = CommentGroup.GetCommentsDataTable(strItemNum);

            dlItemView.DataSource = newCommentGroup;

            dlItemView.DataBind();

        }*/

        

        protected void Button1_Click(object sender, EventArgs e)
        {
            //string tempText = FreeTextBox1.HtmlStrippedText;
            //string tempText1 = FreeTextBox1.Text;
            //string tempText2 = FreeTextBox1.Xhtml;
            //string tempText3 = Server.HtmlDecode(FreeTextBox1.Text);

            //string tempText4 = Server.HtmlEncode(FreeTextBox1.Text);

            FreeTextBox1.Text = "<P style=\"MARGIN: 0in 0in 0pt 0.75in; mso-list: l0 level1 lfo1\" class=MsoListParagraphCxSpFirst><FONT color=#7d7061><SPAN style=\"FONT-FAMILY: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol\"><SPAN style=\"mso-list: Ignore\">�<SPAN style=\"FONT: 7pt 'Times New Roman'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN></SPAN></SPAN><FONT face=Calibri>Test <B style=\"mso-bidi-font-weight: normal\">BOLD</B> <I style=\"mso-bidi-font-style: normal\"><SPAN style=\"mso-tab-count: 1\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN></I><?xml:namespace prefix = o ns = \"urn:schemas-microsoft-com:office:office\" /><o:p></o:p></FONT></FONT></P>\r\n<P style=\"MARGIN: 0in 0in 0pt 0.75in; mso-list: l0 level1 lfo1\" class=MsoListParagraphCxSpMiddle><FONT color=#7d7061><SPAN style=\"FONT-FAMILY: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol\"><SPAN style=\"mso-list: Ignore\">�<SPAN style=\"FONT: 7pt 'Times New Roman'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN></SPAN></SPAN><FONT face=Calibri>Desc Test Bullet<o:p></o:p></FONT></FONT></P>\r\n<P style=\"MARGIN: 0in 0in 0pt 0.75in; mso-list: l0 level1 lfo1\" class=MsoListParagraphCxSpLast><FONT color=#7d7061><SPAN style=\"FONT-FAMILY: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol\"><SPAN style=\"mso-list: Ignore\">�<SPAN style=\"FONT: 7pt 'Times New Roman'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN></SPAN></SPAN><FONT face=Calibri>Test <I style=\"mso-bidi-font-style: normal\">Italic</I></FONT></FONT><o:p></o:p></P>";

            FreeTextBox1.EnableHtmlMode = false;
            FreeTextBox1.EnableViewState = false;
            FreeTextBox1.EnableToolbars = false;
            FreeTextBox1.EnableSsl = false;
            FreeTextBox1.ReadOnly = true;
        }


    }
}
