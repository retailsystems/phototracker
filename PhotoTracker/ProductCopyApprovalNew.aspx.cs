using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;
using System.Text;
using FreeTextBoxControls;



namespace PhotoTracker
{
    public partial class ProductCopyApprovalNew : System.Web.UI.Page
    {
        protected Item theGersTrkItem;
        protected ItemEntry theItemEntryInfo;
        protected ItemEntryGroup theItemEntryGroup;
        protected EmpEntryGroup empEntryGroup;
        protected CommentGroup newCommentGroup;
        public DataTable theItemTopInfo;
        public DataTable dtUser;
        public DataTable dtPermit;
        public string strItemNum = string.Empty;
        public int EmployeeID = 0;
        public string strDiv = string.Empty;
        public string strBarCodeDir = Data.strBarCodePath();
        public string strItmNum = string.Empty;
        public string strZoomedParams = string.Empty;
        public bool bShowImg = false;
        string strPath = ConfigurationManager.AppSettings["strImgDirPath"];
        public string strTopSamp = string.Empty;

        public string strItmPrimaryImg = string.Empty;
        string strImgHTurl= ConfigurationManager.AppSettings["strImgHTurl"];        
        public string errString;
        public string rmsItemNum = string.Empty;
        public string rmsItemDesc = string.Empty;
        public string catalogDesc = string.Empty;
        public string dptNum = string.Empty;
        public string clsNum = string.Empty;
        public string subclsNum = string.Empty;
        public string phTitle = string.Empty;
        public string phDtlDesc = string.Empty;
        public string dtlDescTxt = string.Empty;
        public string primaryIMGPath = string.Empty;
        public string lastUpdateId = string.Empty; 
        public string lastUpdateDate = string.Empty;
        public string lastUpdateDtl = string.Empty;
        public string lastApprDate ;
        public string statusCode = string.Empty;
        SqlDataAdapter da1 = new SqlDataAdapter();
        DataSet ds1 = new DataSet();
        SqlCommand _cmd = new SqlCommand();
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();

               
        public ArrayList listImages = new ArrayList();
        public ArrayList listImgDBSave = new ArrayList();
        public ArrayList listAltImgDBSave = new ArrayList();
        public ArrayList newListImages = new ArrayList();
        public ArrayList newListImagesF = new ArrayList();
        
        public string primeImgTh = string.Empty;
        public string alt1ImgTh  = string.Empty;
        public string alt2ImgTh  = string.Empty;
        public string alt3ImgTh  = string.Empty;
        public string alt4ImgTh  = string.Empty;
        public string alt5ImgTh  = string.Empty;
        public string alt6ImgTh  = string.Empty;
        public string noImgTh    = string.Empty;

        public bool bprimeImgTh = false;
        public bool balt1ImgTh = false;
        public bool balt2ImgTh = false;
        public bool balt3ImgTh = false;
        public bool balt4ImgTh = false;
        public bool balt5ImgTh = false;
        public bool balt6ImgTh = false;
        public bool bnoImgTh = false;

        public string primeImgDB = string.Empty;
        public string alt1ImgDB = string.Empty;
        public string alt2ImgDB = string.Empty;
        public string alt3ImgDB = string.Empty;
        public string alt4ImgDB = string.Empty;
        public string alt5ImgDB = string.Empty;
        public string alt6ImgDB = string.Empty;
        public string noImgDB = string.Empty;

        public string primeImgDBRet = string.Empty;
        public string alt1ImgDBRet = string.Empty;
        public string alt2ImgDBRet = string.Empty;
        public string alt3ImgDBRet = string.Empty;
        public string alt4ImgDBRet = string.Empty;
        public string alt5ImgDBRet = string.Empty;
        public string alt6ImgDBRet = string.Empty;
        public string imgEditIndDBRet = string.Empty;
         
        public void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                strZoomedParams = "?$400x600$"; //"?$35x53$";
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];
   

                //ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {

                    txtItemNum.Visible = true;
                    searchbBtn.Visible = true;
                    BarcodeWebImage2.Visible = false;
                    last_upd_dt_id.Enabled = false;
                    itemNum.Enabled = false;
                    catDesc.Enabled = false;
                    txtRMSDesc.Enabled = false;
                    deptNum.Enabled = false;
                    classNum.Enabled = false;
                    subClassNum.Enabled = false;
                    Image52.Visible = false;
                    itemDtlDesc.Visible = false;
                    itemDtlDesc.Enabled = false;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;
                    itemTitle.Enabled = false;
                    //itemTitle.Enabled = false;
                    //save.Enabled = false;
                    //approve.Enabled = false;
                    unApprove.Enabled = false;
                    //searchbBtn.Enabled = false;
                    ////imgItem.Visible = false;
                    dtlDescEdit.Visible = false;
                    titleEdit.Visible = false;

                    //primeImgPath.Visible = false;
                    //altImg1.Visible = false;
                    //altImg2.Visible = false;
                    //altImg3.Visible = false;
                    //altImg4.Visible = false;
                    //altImg5.Visible = false;
                    //altImg6.Visible = false;
                    //noImg.Visible = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }

        }


        private bool ImageExists(string imgURL)
        {
            WebRequest webRequest = WebRequest.Create(imgURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }
        
        
        private void ClearExpItmTxt()
        {
            itemNum.Visible = false;
            catDesc.Visible = false;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Visible = false;
            last_upd_dt_id.Visible = false;
            apprDate.Visible = false;
            itemTitle.Visible = false;
            itemDtlDesc.Visible = false;
            deptNum.Visible = false;
        }

        private void EmtyExpItmTxt()
        {
            itemNum.Text = string.Empty;
            catDesc.Text = string.Empty;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Text = string.Empty;
            last_upd_dt_id.Text = string.Empty;            
            apprDate.Text = string.Empty;
            itemTitle.Text = string.Empty;
            itemDtlDesc.Text = string.Empty;
            deptNum.Text = string.Empty;
            classNum.Text = string.Empty;
            primeImgPath.Text = string.Empty;
            subClassNum.Text = string.Empty;
            FreeTextBox1.Text = string.Empty;
            FreeTextBox1.EnableHtmlMode = false;
            FreeTextBox1.EnableViewState = false;
            FreeTextBox1.EnableToolbars = false;
            FreeTextBox1.EnableSsl = false;
            FreeTextBox1.ReadOnly = true;
        }


        private void EnableTxt(object sender, EventArgs e)
        {
            itemNum.Visible = true;
            catDesc.Visible = true;
            BarcodeWebImage2.Visible = true;
            txtRMSDesc.Visible = true;
            last_upd_dt_id.Visible = true;
            apprDate.Visible = true;
            itemTitle.Visible = true;
            //itemDtlDesc.Visible = true;
            deptNum.Visible = true;
            classNum.Visible = true;
            skuGrdView.Visible = true;
            //primeImgPath.Visible = true;
            subClassNum.Visible = true;

        }

        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            ClearExpItmTxt();
        }


        private void LoadEntryTime(TextBox txtTimeBox, string strTime)
        {
            txtTimeBox.Text = string.Empty;
            txtTimeBox.Text = strTime;

            if (strTime != "")
            {
                txtTimeBox.Enabled = false;

            }
        }
        private void LoadRecvName(TextBox txtRecNameBox, string strName)
        {
            txtRecNameBox.Text = string.Empty;
            txtRecNameBox.Text = strName;

            if (strName != "")
            {
                txtRecNameBox.Enabled = false;

            }
        }
        private void LoadDropdownName(DropDownList ddlList, string strName, bool IsSelected)
        {
            ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            if (IsSelected)
            {
                ddlList.Enabled = false;

            }
        }
        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }

        
        
        
        protected int GetItemDtl(string strItemNum, ref string strErrorMsg)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;            

            int returnCode = 0;

            strErrorMsg = string.Empty;

            try
            {

                conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                conn.Open();
                cmd = new SqlCommand("SELECT im.Style,im.description rmsItmDesc, ci.description catDesc, " +
                                     "im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath,im.altIMG1,im.altIMG2,im.altIMG3,im.altIMG4,im.altIMG5,im.altIMG6, im.lastphupdateid, " +
                                     " im.ImgEdit, im.lastphupdatedate,im.lastphapprdate,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc FROM [RMS_PH_STYLE_Master] im " +
                                     " INNER JOIN [Catalog_Id] ci ON im.division = ci.division where " +
                                     "  (im.Style='" + strItemNum + "' or  im.Style in (select Program from [RMS_DW_ITEM_MASTER] im1 " + //need to change the sku table name
                                     " where im1.sku='" + strItemNum + "')) ", conn); 

                SqlDataReader reader = cmd.ExecuteReader();
                
                //conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                //conn.Open();
                /*cmd = new SqlCommand("SELECT im.itemnum,im.description rmsItmDesc, [Catalog_Id].description catDesc FROM [RMS_Item_Master] im " +
                                     " INNER JOIN [Catalog_Id] ON im.division = [Catalog_Id].division where im.Program is null " +
                                     " and (im.itemnum='" + strItemNum + "' or  im.itemnum in (select Program from [RMS_Item_Master] im1 " +
                                     " where im1.itemnum='" + strItemNum + "')) ", conn); */

                //reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    /*if (reader["Style"].ToString() != null && reader["Style"].ToString() != string.Empty)
                    {
                        rmsItemNum = reader["Style"].ToString();
                    }
                    if (reader["rmsItmDesc"].ToString() != null && reader["rmsItmDesc"].ToString() != string.Empty)
                    {
                        rmsItemDesc = reader["rmsItmDesc"].ToString();
                    }
                    if (reader["catDesc"].ToString() != null && reader["catDesc"].ToString() != string.Empty)
                    {
                        catalogDesc = reader["catDesc"].ToString();
                    }*/
                    rmsItemNum     = reader["Style"].ToString();
                    rmsItemDesc    = reader["rmsItmDesc"].ToString();
                    catalogDesc    = reader["catDesc"].ToString();
                    dptNum         = reader["department"].ToString();
                    clsNum         = reader["class"].ToString();
                    subclsNum      = reader["subclass"].ToString();
                    phTitle        = reader["phtitle"].ToString();

                    primeImgDBRet = reader["primaryimgpath"].ToString();
                    alt1ImgDBRet = reader["altIMG1"].ToString();
                    alt2ImgDBRet = reader["altIMG2"].ToString();
                    alt3ImgDBRet = reader["altIMG3"].ToString();
                    alt4ImgDBRet = reader["altIMG4"].ToString();
                    alt5ImgDBRet = reader["altIMG5"].ToString();
                    alt6ImgDBRet = reader["altIMG6"].ToString();
                    imgEditIndDBRet = reader["ImgEdit"].ToString();

                    phDtlDesc = reader["dtlDesc"].ToString();
                    if (reader["lastphupdatedate"].ToString() != null && reader["lastphupdatedate"].ToString() != string.Empty)
                    {
                        //lastUpdateDate = FilterDate(Convert.ToDateTime(reader["lastphupdatedate"].ToString()));
                        lastUpdateDate = reader["lastphupdatedate"].ToString();
                    }
                    if (reader["lastphupdateid"].ToString() != null && reader["lastphupdateid"].ToString() != string.Empty)
                    {
                        lastUpdateId = reader["lastphupdateid"].ToString();
                    }
                    if ((lastUpdateDate == null || lastUpdateDate == string.Empty) && (lastUpdateDtl == null || lastUpdateDtl == string.Empty))
                    {
                        lastUpdateDtl = string.Empty;
                    }
                    else
                    {
                        lastUpdateDtl = "[" + lastUpdateId + "," + lastUpdateDate + "]";
                    }

                    if (reader["lastphapprdate"].ToString() != null && reader["lastphapprdate"].ToString() != string.Empty)
                    {
                        lastApprDate = FilterDate(Convert.ToDateTime(reader["lastphapprdate"].ToString()));
                    }

                    statusCode = reader["status"].ToString();
                    dtlDescTxt = reader["LongDesc"].ToString();

                }

                cmd.Dispose();
                conn.Close();

                returnCode = 0;
            }
            catch (Exception ex)
            {
                strErrorMsg = ex.Message;
                System.Diagnostics.EventLog.WriteEntry("Photocopyapproval", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                //LogError("An error occurred in GetItemDtl function while fetching the item details " + ex.ToString());
                returnCode = -1;
            }

            return returnCode;
        }

        //Deba
        protected void searchBtnClicked(object sender, EventArgs e)
        {
            bool bItem = false;
            //lblNoItemMsg.Text = string.Empty;
            errString = string.Empty;
            int intRevResult = 0;
            int intItemExixt = 0;
            int intRetCode = 0;

            //ClearExpItmTxt();
            lblNoItemMsg.Text=string.Empty;
            EmtyExpItmTxt();

            strItemNum = this.txtItemNum.Text.Trim();

            if (strItemNum != "")
            {
                ViewState["StrItemNum"] = strItemNum;

                intRetCode = GetItemDtl(strItemNum, ref errString);

                if (intRetCode == 0)
                {
                    itemNum.Text             = rmsItemNum;
                    txtRMSDesc.Text          = rmsItemDesc;
                    catDesc.Text             = catalogDesc;
                    BarcodeWebImage2.Visible = true;
                    BarcodeWebImage2.Value = rmsItemNum;
                    //primeImgPath.Text = primaryIMGPath;
                    last_upd_dt_id.Text = lastUpdateDtl;
                    apprDate.Text = lastApprDate;
                    itemTitle.Text = phTitle;
                    //itemDtlDesc.Text=phDtlDesc;
                    FreeTextBox1.Text = dtlDescTxt;
                    itemDtlDesc.Text = dtlDescTxt;
                    FreeTextBox1.ReadOnly = true;
                    deptNum.Text = dptNum;
                    classNum.Text = clsNum;
                    subClassNum.Text = subclsNum;
                    dtlDescEdit.Visible = true;
                    titleEdit.Visible = true;
                    skuBindData(rmsItemNum);
                    skuGrdView.Visible = true;
                    
                    primaryIMGPath=primeImgDBRet;
                    //
                    if (imgEditIndDBRet == "Y")
                    {
                        newListImagesF.Add(primeImgDBRet);
                        newListImagesF.Add(alt1ImgDBRet);
                        newListImagesF.Add(alt2ImgDBRet);
                        newListImagesF.Add(alt3ImgDBRet);
                        newListImagesF.Add(alt4ImgDBRet);
                        newListImagesF.Add(alt5ImgDBRet);
                        newListImagesF.Add(alt6ImgDBRet);
                    }
                    else
                    {
                        if (primaryIMGPath != null && primaryIMGPath != string.Empty)
                        {
                            strItmPrimaryImg = strImgHTurl + primaryIMGPath;
                        }
                        //ShowItmImage(rmsItemNum);
                        ShowImage(rmsItemNum);
                    }
                    setAltIMG();
                    bnoImgTh = true;

                    if (statusCode == "APPROVED")
                    {
                        unApprove.Enabled = true;
                        approve.Enabled = false;
                    }
                    else if ((statusCode == "SAVED" || statusCode == "UNAPPROVED") && (FreeTextBox1.Text != string.Empty && itemTitle.Text!=string.Empty))
                    {
                        approve.Enabled = true;
                    }
                }
                else
                {
                    lblNoItemMsg.Text = "Item # " + strItemNum + " does not exist. Please re-enter.";
                    //ClearAll();
                    EmtyExpItmTxt();
                }
                
            }
            else
            {
                ClearAll();
                //pnlRecv.Visible = false;
                //pnlUpdate.Visible = true;
                //pnlEntry.Visible = false;
                lblNoItemMsg.Text = "You must input Item Number";
            }
        }
        //Deba

      
        /*private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            strZoomedParams = "?$400x600$";
            strItemImage = ConfigurationManager.AppSettings["strItemImage"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit"];
            imgDBItem = imgDB.Replace("[picvalue]", strItemNum);
 
            if (strItmPrimaryImg != string.Empty)
            {
                strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL;
                }
            }
                       
            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                listImages.Add("_hi");
                listImgDBSave.Add(imgDBItem+"_hi");

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                        listImgDBSave.Add(imgDBItem + strSml);
                    }
                }
                bShowImg = true;

                if (!listImgDBSave.Contains(primaryIMGPath.Replace("/is/image/HotTopic/","")))
                {
                    //listImages.Add();
                }
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }*/


        private void ShowImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            strZoomedParams = "?$400x600$";
            strItemImage = ConfigurationManager.AppSettings["strImgPathSplit1"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            //imgDB = ConfigurationManager.AppSettings["strImgPathSplit"];
            //imgDBItem = imgDB.Replace("[picvalue]", strItemNum);

            if (strItmPrimaryImg != string.Empty)
            {
                //strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL;
                }
            }

            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                newListImages.Add(strURL);

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        newListImages.Add(strSmlURL);
                    }
                }
                bShowImg = true;

                /*if (!newListImages.Contains(primaryIMGPath.Replace("/is/image/HotTopic/", "")))
                {
                    newListImages.Add(primaryIMGPath);
                }*/
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }

        private void setAltIMG()
        {
            
            for (int i=0;i<newListImages.Count;i++)
            {

                if (i == 0)
                {
                    primeImgTh = newListImages[0].ToString();
                    primeImgPath.Text = primeImgTh;
                    bprimeImgTh = true;
                }
                if (i == 1)
                {
                    alt1ImgTh = newListImages[1].ToString();
                    altImg1.Text = alt1ImgTh;
                    balt1ImgTh = true;
                }
                if (i == 2)
                {

                    alt2ImgTh = newListImages[2].ToString();
                    altImg2.Text = alt2ImgTh;
                    balt2ImgTh = true;
                }
                if (i == 3)
                {

                    alt3ImgTh = newListImages[3].ToString();
                    altImg3.Text = alt3ImgTh;
                    balt3ImgTh = true;
                }
                if (i == 4)
                {
                    alt4ImgTh = newListImages[4].ToString();
                    altImg4.Text = alt4ImgTh;
                    balt4ImgTh = true;
                }
                if (i == 5)
                {
                    alt5ImgTh = newListImages[5].ToString();
                    altImg5.Text = alt5ImgTh;
                    balt5ImgTh = true;
                }
                if (i == 6)
                {

                    alt6ImgTh = newListImages[6].ToString();
                    altImg6.Text = alt6ImgTh;
                    balt6ImgTh = true;
                }

                //noImgTh = newListImages[7].ToString();
                //noImg.Text = noImgTh;
            }

        }

        private void setImgForDB()
        {

            if (primeImgPath.Text!=string.Empty)
            {
                primeImgDB = primeImgPath.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg1.Text != string.Empty)
            {
                alt1ImgDB = altImg1.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg2.Text != string.Empty)
            {
                alt2ImgDB = altImg2.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg3.Text != string.Empty)
            {
                alt3ImgDB = altImg3.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg4.Text != string.Empty)
            {
                alt4ImgDB = altImg4.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg5.Text != string.Empty)
            {
                alt5ImgDB = altImg5.Text.Replace("http://img.hottopic.com", "");
            }
            if (altImg6.Text != string.Empty)
            {
                alt6ImgDB = altImg6.Text.Replace("http://img.hottopic.com", "");
            }

            if (noImg.Text != string.Empty)
            {
                if (primeImgDB == string.Empty)
                {
                    primeImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt1ImgDB == string.Empty)
                {
                    alt1ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt2ImgDB == string.Empty)
                {
                    alt2ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt3ImgDB == string.Empty)
                {
                    alt3ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt4ImgDB == string.Empty)
                {
                    alt4ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt5ImgDB == string.Empty)
                {
                    alt5ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else if (alt6ImgDB == string.Empty)
                {
                    alt6ImgDB = noImg.Text.Replace("http://img.hottopic.com", "");
                }
                else
                {
                    lblNoItemMsg.Text = "You can't enter more than 7 images per style";
                }
            }
            

        }

        private void skuBindData(string strRMSItemNum)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String skuSql=string.Empty;
            try
            {
                Sqlconn.Open();
                skuSql = "select sku skuNum,size sizeId,color colorId,ats ats,unit_retail price from [RMS_DW_ITEM_MASTER] where program='" + strRMSItemNum + "' order by sku";

                da1.SelectCommand = new SqlCommand(skuSql, Sqlconn);
                da1.Fill(ds1, "tblSkuDtl");
                skuGrdView.DataSource = ds1.Tables["tblSkuDtl"];
                skuGrdView.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }

        protected void titleEdit_Click(object sender, EventArgs e)
        {
            /*if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                strZoomedParams = "?$215x219$"; //"?$35x53$";
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];


                //ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {*/
                    itemTitle.Enabled = true;
                    /*}
                    else
                    {
                        lblNoItemMsg.Text = "You need Edit permission to do the changes";
                    }
                }*/

        }


        protected void titleEdit_Click1(object sender, EventArgs e)
        {
            itemTitle.Enabled = true;
            if (strItmPrimaryImg == string.Empty)
            {
                if (this.primeImgPath.Text != null && this.primeImgPath.Text != string.Empty)
                {
                    strItmPrimaryImg = strImgHTurl + this.primeImgPath.Text;
                }
                //ShowItmImage(this.itemNum.Text);
                ShowImage(this.itemNum.Text);
                setAltIMG();
            }
        }

        protected void dtlDescEdit_Click(object sender, EventArgs e)
        {
            //itemDtlDesc.Enabled = true;
            FreeTextBox1.EnableHtmlMode = true;
            FreeTextBox1.EnableViewState = true;
            FreeTextBox1.EnableToolbars = true;
            FreeTextBox1.EnableSsl = true;
            FreeTextBox1.ReadOnly = false;
            FreeTextBox1.Text = itemDtlDesc.Text;
            itemDtlDesc.Text = string.Empty;
            if (strItmPrimaryImg == string.Empty)
            {
                if (this.primeImgPath.Text != null && this.primeImgPath.Text != string.Empty)
                {
                    strItmPrimaryImg = strImgHTurl + this.primeImgPath.Text;
                }
                //ShowItmImage(this.itemNum.Text);
                ShowImage(this.itemNum.Text);
                setAltIMG();
            }

        }

        protected string get_UserID()
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
            string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
            string userId = strFirstName.Substring(0, 1) + strLastName;

            return userId;
        }

        protected void save_Click(object sender, EventArgs e)
        {

            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;
            String imgEditInd =string.Empty;

            //String tempFreeText = this;
            //String tempFreeText = FreeTextBox1.

            String lastUpdId = apprDate.Text;
            String tempText1 = FreeTextBox1.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId=get_UserID();

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }

            try
            {
                setImgForDB();

                if (lblNoItemMsg.Text == string.Empty)
                {

                    //EnableTxt();
                    if ((newItmTitle == null || newItmTitle == string.Empty) && (tempText1 == null || tempText1 == string.Empty) && (this.primeImgPath.Text == null || this.primeImgPath.Text == string.Empty))
                    {
                        //EnableTxt();
                        lblNoItemMsg.Text = "Item Enrichment can't be saved without Title , Detail description or Image Path change";
                        //pnlButton.Visible = true;
                    }

                    else
                    {
                        if (primeImgDB!=string.Empty)
                        {
                            imgEditInd = "Y";
                        }
                        else
                        {
                            imgEditInd = "N";
                        }
                        String statusSql = string.Empty;
                        byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                        //update the status and Title
                        /*statusSql = "update RMS_PH_STYLE_Master set status='SAVED',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg,"+
                            "dtlDesc=@dtlDesctxt,temp_dtl_txt=@dtl_text" +
                            " where style='" + this.itemNum.Text + "'";*/

                        statusSql = "update RMS_PH_STYLE_Master set status='SAVED',publishedInd='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg," +
                            "dtl_Desc_txt=@dtldesctext,altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,ImgEdit=@imgED" +
                            " where style='" + this.itemNum.Text + "'";
                        Sqlconn.Open();
                        _cmd = new SqlCommand(statusSql, Sqlconn);
                        _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                        _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                        _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                        /*if (lastApprDT == null || lastApprDT == string.Empty)
                        {
                            _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now );
                        }
                        else
                        {
                            _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                        }*/
                        //_cmd.Parameters.AddWithValue("@primImg", this.primeImgPath.Text);
                        //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                        _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                        //_cmd.Parameters.AddWithValue("@dtlDesctxt", string.Empty);
                        //_cmd.Parameters.AddWithValue("@dtl_text", dtlDesctxt); 

                        _cmd.Parameters.AddWithValue("@primImg", primeImgDB);
                        _cmd.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                        _cmd.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                        _cmd.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                        _cmd.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                        _cmd.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                        _cmd.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                        _cmd.Parameters.AddWithValue("@imgED", imgEditInd);
                        _cmd.CommandType = CommandType.Text;
                        _cmd.ExecuteNonQuery();

                        lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "saved succssfully";
                        EmtyExpItmTxt();
                        txtItemNum.Text = string.Empty;
                        skuGrdView.Visible = false;

                    }



                }
            }

            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender,e);
            //ShowItmImage(this.itemNum.Text);
            /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
            {
                approve.Enabled = true;
            }*/

        }

        protected void approve_Click(object sender, EventArgs e)
        {
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;
            String imgEditInd = string.Empty;

            String lastUpdId = apprDate.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }

            try
            {
                setImgForDB();

                if (lblNoItemMsg.Text == string.Empty)
                {
                    //EnableTxt();
                    if ((newItmTitle == null || newItmTitle == string.Empty) || (tempText1 == null || tempText1 == string.Empty))
                    {
                        //EnableTxt();
                        lblNoItemMsg.Text = "Item Enrichment can't be approved without Title and Detail description";
                        ShowImage(this.itemNum.Text);
                        //pnlButton.Visible = true;
                    }

                    else
                    {
                        if (primeImgDB!=string.Empty)
                        {
                            imgEditInd = "Y";
                        }
                        else
                        {
                            imgEditInd = "N";
                        }

                        String statusSql = string.Empty;
                        byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                        //update the status and Title
                        statusSql = "update RMS_PH_STYLE_Master set status='APPROVED',publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprID=@lastApprID"+
                            ",altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,ImgEdit=@imgED" +
                            " where style='" + this.itemNum.Text + "'";

                        Sqlconn.Open();
                        _cmd = new SqlCommand(statusSql, Sqlconn);
                        _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                        _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                        _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                        if (lastApprDT == null || lastApprDT == string.Empty)
                        {
                            _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now);
                        }
                        else
                        {
                            _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                        }
                        //_cmd.Parameters.AddWithValue("@primImg", this.primeImgPath.Text);
                        //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                        _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                        _cmd.Parameters.AddWithValue("@lastApprID", lastUpdId);

                        _cmd.Parameters.AddWithValue("@primImg", primeImgDB);
                        _cmd.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                        _cmd.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                        _cmd.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                        _cmd.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                        _cmd.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                        _cmd.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                        _cmd.Parameters.AddWithValue("@imgED", imgEditInd);

                        _cmd.CommandType = CommandType.Text;
                        try
                        {
                            _cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.Message);
                        }
                        finally
                        {
                            Sqlconn.Close();
                            Sqlconn.Dispose();
                        }

                        lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "approved succssfully";
                        EmtyExpItmTxt();
                        txtItemNum.Text = string.Empty;
                        skuGrdView.Visible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
            //ShowItmImage(this.itemNum.Text);
        }

        protected void unApprove_Click(object sender, EventArgs e)
        {
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;

            String lastUpdId = apprDate.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }


            try
            {

                setImgForDB();

                if (lblNoItemMsg.Text == string.Empty)
                {
                    String statusSql = string.Empty;
                    byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                    //update the status and Title
                    statusSql = "update RMS_PH_STYLE_Master set status='UNAPPROVED',publishedInd='N',Online_Flag='N',LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,LastPHApprID=@lastApprID" +
                        " where style='" + this.itemNum.Text + "'";

                    Sqlconn.Open();
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@lastApprDT", string.Empty);
                    _cmd.Parameters.AddWithValue("@lastApprID", string.Empty);

                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();

                    lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "unapproved succssfully";
                    EmtyExpItmTxt();
                    txtItemNum.Text = string.Empty;
                    skuGrdView.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
                //ShowItmImage(this.itemNum.Text);
                /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
                {
                    approve.Enabled = true;
                }*/
                

        }

        protected void textChanged1(object sender, EventArgs e)
        {
            if (itemTitle.Text != null && itemTitle.Text != string.Empty )
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowImage(this.itemNum.Text);
        }
        protected void textChanged2(object sender, EventArgs e)
        {
            if (primeImgPath.Text != null && primeImgPath.Text != string.Empty)
            {
                save.Enabled = true;
            }
            ShowImage(this.itemNum.Text);
        }
        protected void textChanged3(object sender, EventArgs e)
        {
            if (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty)
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowImage(this.itemNum.Text);
        }

        /*protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {
            if (txtItemNum.Text != null && txtItemNum.Text != string.Empty)
            {
                searchbBtn.Enabled = true;
            }
            else
            {
                searchbBtn.Enabled = false;
            }
            ShowItmImage(this.itemNum.Text);
        }*/

    }
}
