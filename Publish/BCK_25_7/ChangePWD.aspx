<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ChangePWD.aspx.cs" Inherits="PhotoTracker.ChangePassword" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<HEAD>
		<title>ChangeMyPassword</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
 
</HEAD>
	<body onload="javascript:document.Form1.txtEmpID.focus();" bottomMargin="0" bgColor="#000000" topMargin="0">
		<div id="header">
          
		<div id="main">
			<form id="Form1" method="post" runat="server">
			    <uc1:header id="Header1" runat="server"></uc1:header>
				<br>
				<TABLE id="tblLogin" class="innerTableLog" cellSpacing="0" cellPadding="1" width="96%"
					height="380" align="center" border="0">
					<tr>
						<td>
							 <table id = "tblChgPwd"  runat="server"  align="center" style="z-index: 100; left: 298px; position: absolute; top: 81px; border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; width: 443px; border-bottom: black thin solid; text-align: right;">
                                <tr>
                                    <td colspan="2" style="border-bottom: black thin solid; height: 37px; text-align: center">
                                        <strong><span style="color: red; font-size: 14pt;">TRACKING MENU</span></strong></td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; height: 25px; text-align: right">
                                    </td>
                                    <td style="width: 100px; height: 25px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; text-align: right;">
                                        <strong>
                                        Enter User ID: </strong>
                                     </td>
                                    <td style="width: 100px; text-align: left;">
                                        <asp:TextBox ID="txtEmpID"  Width="175px"  Height="22px"  MaxLength="6" runat="server">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; text-align: right;">
                                        <strong>
                                        Enter an Old assword: </strong>
                                    </td>
                                    <td style="width: 100px; text-align: left;">
                                        <asp:TextBox ID="txtOldPwd"    TextMode="password"   Width="175px"  Height="22px" runat="server">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; text-align: right;">
                                        <strong>Enter New Password: </strong>
                                    </td>
                                    <td style="width: 100px; text-align: left;">
                                        <asp:TextBox ID="txtNewPwd"    TextMode="Password" Width = "175px"  Height="22px" runat="server">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; text-align: right; height: 40px;">
                                        <strong>
                                        Confirm New Password: </strong>
                                    </td>
                                    <td style="width: 100px; height: 40px; text-align: left;">
                                        <asp:TextBox ID="txtConfirmPwd"   TextMode="Password" Width= "175px"  Height="22px" runat="server">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px; height: 15px;">
                                    </td>
                                    <td style="width: 100px; height: 15px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 141px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                            </table>
                                <asp:Button ID="btnSave" Text="Save" Width="100px"  AccessKey="s" runat="server" style="z-index: 100; left: 344px; position: absolute; top: 362px" ToolTip="Access Key - Alt+s" OnClick="btnSave_Click" ValidationGroup="btnSavePwd" />
                            &nbsp;
                                <asp:Button ID="btnExit" Text="Exit" Width="100px"  AccessKey="x" runat="server" style="z-index: 101; left: 565px; position: absolute; top: 365px" ToolTip="Access Key - Alt+x" OnClick="btnExit_Click" />
                             <asp:Label ID="lblPwdChangeResult"   Width = "598px" runat="server" style="z-index: 102; left: 215px; position: absolute; top: 406px" Height="23px" Font-Size="14pt" ></asp:Label>
                        &nbsp;
                             
                        &nbsp;&nbsp;&nbsp;
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Error changing your passwod."
                                Style="z-index: 104; left: 342px; position: absolute; top: 464px" ValidationGroup="btnSavePwd"
                                Width="346px" />
				<table height="30" cellSpacing="0" cellPadding="1" width="96%" align="center" border="0">
					<tr>
						<td vAlign="top" align="right"></td>
					</tr>
				</table>
			</form>
		<br />
            </TD></TR></TBODY></TABLE><br /><br /></div></div>
	</body>
</HTML>

